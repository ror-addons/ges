
	--Define the constant values that can be used in the other LUA scripts of the addon.
	GesConstants = {}

	GesConstants.CAST_SUCCESS = 1
	GesConstants.CAST_FAILED  = 2
	GesConstants.COMMS_DATA_ADDON       = 1
	GesConstants.COMMS_DATA_SEND_ID    = 2
	GesConstants.COMMS_DATA_STATUS      = 3
	GesConstants.COMMS_DATA_CASTER_ID   = 4
	GesConstants.COMMS_DATA_CASTER_NAME = 5
	GesConstants.COMMS_DATA_TARGET_ID   = 6
	GesConstants.COMMS_DATA_TARGET_NAME = 7
	GesConstants.COMMS_DATA_STATE       = 8
	GesConstants.COMMS_TYPE_SEND    = 1
	GesConstants.COMMS_TYPE_ANOUNCE = 2
	GesConstants.COMMS_SEND_DATA = 1
	GesConstants.COMMS_SEND_TYPE = 2
	GesConstants.COMMS_THROTTLE  = 1
	GesConstants.COMMS_SEND_THROTTLE  = 3
	GesConstants.COMMS_ANNOUNCE_THROTTLE = 1
	GesConstants.EXTRAS_TARGET_HEALTH = 1
	GesConstants.EXTRAS_TARGET_NAME   = 2
	GesConstants.EXTRAS_TARGET_CAREER = 3
	GesConstants.FOREGROUND_HILIGHT = 1
	GesConstants.FOREGROUND_TEXTBG  = 2
	GesConstants.FOREGROUND_TEXTDC  = 3
	GesConstants.FOREGROUND_SKULL   = 4
	GesConstants.FOREGROUND_BLANK   = 5
	GesConstants.ICON_MONITOR   = 1
	GesConstants.ICON_TRACKER   = 2
	GesConstants.SELECTION_DISABLED = 1
	GesConstants.SOUNDS_PREF_1  = 1
	GesConstants.SOUNDS_PREF_2  = 2
	GesConstants.SOUNDS_PREF_3  = 3
	GesConstants.SOUNDS_PREF_4  = 4	
	GesConstants.STATUS_APPLIED = 1
	GesConstants.STATUS_REMOVED = 2
	GesConstants.TAB_GENERAL_ID = 1
	GesConstants.TAB_COLOUR_ID  = 2
	GesConstants.TAB_EXTRAS_ID  = 3
	GesConstants.TAB_SOUNDS_ID  = 4
	
	GesConstants.AlertText = {
	  { id = nil                                                   , name = "Do not alert" }
	, { id = SystemData.AlertText.Types.DEFAULT                    , name = "Default" }
	, { id = SystemData.AlertText.Types.COMBAT                     , name = "Combat" }
	, { id = SystemData.AlertText.Types.QUEST_NAME                 , name = "Quest Name" }
	, { id = SystemData.AlertText.Types.QUEST_CONDITION            , name = "Quest Condition" }
	, { id = SystemData.AlertText.Types.QUEST_END                  , name = "Quest End" }
	, { id = SystemData.AlertText.Types.OBJECTIVE                  , name = "Objective" }
	, { id = SystemData.AlertText.Types.RVR                        , name = "RvR" }
	, { id = SystemData.AlertText.Types.SCENARIO                   , name = "Scenario" }
	, { id = SystemData.AlertText.Types.MOVEMENT_RVR               , name = "Movement RvR" }
	, { id = SystemData.AlertText.Types.ENTERAREA                  , name = "Enter Area" }
	, { id = SystemData.AlertText.Types.STATUS_ERRORS              , name = "Status Errors" }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_GOLD   , name = "Status Achievements Gold"   }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_PURPLE , name = "Status Achievements Purple" }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_RANK   , name = "Status Achievements Rank"   }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_RENOUN , name = "Status Achievements Renown" }
	, { id = SystemData.AlertText.Types.PQ_ENTER                   , name = "PQ Enter" }
	, { id = SystemData.AlertText.Types.PQ_NAME                    , name = "PQ Name" }
	, { id = SystemData.AlertText.Types.PQ_DESCRIPTION             , name = "PQ Description" }
	, { id = SystemData.AlertText.Types.ENTERZONE                  , name = "Enter Zone" }
	, { id = SystemData.AlertText.Types.ORDER                      , name = "Order" }
	, { id = SystemData.AlertText.Types.DESTRUCTION                , name = "Destruction" }
	, { id = SystemData.AlertText.Types.NEUTRAL                    , name = "Neutral" }
	, { id = SystemData.AlertText.Types.ABILITY                    , name = "Ability" }
	, { id = SystemData.AlertText.Types.BO_ENTER                   , name = "BO Enter" }
	, { id = SystemData.AlertText.Types.BO_NAME                    , name = "BO Name" }
	, { id = SystemData.AlertText.Types.BO_DESCRIPTION             , name = "BO Description" }
	, { id = SystemData.AlertText.Types.ENTER_CITY                 , name = "Enter City" }
	, { id = SystemData.AlertText.Types.CITY_RATING                , name = "City Rating" }
	, { id = SystemData.AlertText.Types.GUILD_RANK                 , name = "Guild Rank" }
	, { id = SystemData.AlertText.Types.RRQ_UNPAUSED               , name = "RRQ Unpaused" }
	, { id = SystemData.AlertText.Types.LARGE_ORDER                , name = "Large Order" }
	, { id = SystemData.AlertText.Types.LARGE_DESTRUCTION          , name = "Large Destruction" }
	, { id = SystemData.AlertText.Types.LARGE_NEUTRAL              , name = "Large Neutral" }
	                         }

	GesConstants.Anchors  = {
	  { name = "Top"   , opposite = "Bottom" , offsetX = -3 , offsetY = -3}
	, { name = "Left"  , opposite = "Right"  , offsetX = -5 , offsetY = -5}
	, { name = "Center", opposite = "Center" , offsetX = -3 , offsetY = -5}
	, { name = "Right" , opposite = "Left"   , offsetX =  0 , offsetY = -5}
	, { name = "Bottom", opposite = "Top"    , offsetX = -3 , offsetY =  0}
	                         }
							 
	GesConstants.Channels = {
		  { channel = "sm" , desc = "Smart Channel"  , id = SystemData.ChatLogFilters.SAY}
		, { channel = "p"  , desc = "Party"          , id = SystemData.ChatLogFilters.GROUP}
		, { channel = "wb" , desc = "Warband"        , id = SystemData.ChatLogFilters.BATTLEGROUP}
		, { channel = "sc" , desc = "Scenario"       , id = SystemData.ChatLogFilters.SCENARIO}
		, { channel = "s"  , desc = "Say"            , id = SystemData.ChatLogFilters.SAY}
		, { channel = "sh" , desc = "Shout"          , id = SystemData.ChatLogFilters.SHOUT}
		, { channel = "g"  , desc = "Guild"          , id = SystemData.ChatLogFilters.GUILD}
		, { channel = "1"  , desc = "Region 1"       , id = SystemData.ChatLogFilters.CHANNEL_1}
		, { channel = "2"  , desc = "Region 2"       , id = SystemData.ChatLogFilters.CHANNEL_2}
		, { channel = "t1" , desc = "Tier 1"         , id = SystemData.ChatLogFilters.REALM_WAR_T1}
		, { channel = "t2" , desc = "Tier 2"         , id = SystemData.ChatLogFilters.REALM_WAR_T2}
		, { channel = "t3" , desc = "Tier 3"         , id = SystemData.ChatLogFilters.REALM_WAR_T3}
		, { channel = "t4" , desc = "Tier 4"         , id = SystemData.ChatLogFilters.REALM_WAR_T4}
		, { channel = ""   , desc = "Don't Announce" , id = SystemData.ChatLogFilters.SAY}
	                        }
				   
	GesConstants.Features = {
	  [GesConstants.EXTRAS_TARGET_HEALTH] = { window = "TargetHealth" , name = "Target Health"}
	, [GesConstants.EXTRAS_TARGET_NAME]   = { window = "TargetName"   , name = "Target Name"  }
	, [GesConstants.EXTRAS_TARGET_CAREER] = { window = "CareerIcon"   , name = "Career Icon"  }
	                        }
	
	GesConstants.Icons = {
	   [GesConstants.ICON_MONITOR] = { name = "GesMonitor" , label = "Monitor" , altered = false }
	,  [GesConstants.ICON_TRACKER] = { name = "GesTracker" , label = "Tracker" , altered = false }
	                     }
						
	GesConstants.IconsBackgrounds = {
		  { texture = "GesBackTexture" , slice = "Grey"   , name = "Grey"  }
		, { texture = "GesBackTexture" , slice = "Orange" , name = "Orange"}
		, { texture = "GesBackTexture" , slice = "Yellow" , name = "Yellow"}
		, { texture = "GesBackTexture" , slice = "Green"  , name = "Green" }
		, { texture = "GesBackTexture" , slice = "Red"    , name = "Red"   }
		, { texture = "GesBackTexture" , slice = "Blue"   , name = "Blue"  }
		, { texture = "GesBackTexture" , slice = "Purple" , name = "Purple"}
		, { texture = "GesBackTexture" , slice = "Pink"   , name = "Pink"  }
		, { texture = "GesBackTexture" , slice = "Black"  , name = "Black" }
		                            }
						   
	GesConstants.IconsForegrounds = {
		  { texture = "GesForeTexture" , slice = "Hilight" , name = "Hilight"}
		, { texture = "GesForeTexture" , slice = "TextBG"  , name = "TextBG" }
		, { texture = "GesForeTexture" , slice = "TextDC"  , name = "TextDC" }
		, { texture = "GesForeTexture" , slice = "Skull"   , name = "Skull"  }
		, { texture = "GesForeTexture" , slice = "Blank"   , name = "Blank"  }
	                                }						
	
	GesConstants.SoundTypes = {
	  { id = nil                                           , name = "Do not play a sound"}
	, { id = GameData.Sound.ACTION_FAILED                  , name = "Action Failed"}
	, { id = GameData.Sound.ADVANCE_RANK                   , name = "Advance Rank"}
	, { id = GameData.Sound.ADVANCE_TIER                   , name = "Advance Tier"}
	, { id = GameData.Sound.APOTHECARY_ADD_FAILED          , name = "Apothecary Add Failed"}
	, { id = GameData.Sound.APOTHECARY_BREW_STARTED        , name = "Apothecary Brew Started"}
	, { id = GameData.Sound.APOTHECARY_CONTAINER_ADDED     , name = "Apothecary Container Added"}
	, { id = GameData.Sound.APOTHECARY_DETERMINENT_ADDED   , name = "Apothecary Determinent Added"}
	, { id = GameData.Sound.APOTHECARY_FAILED              , name = "Apothecary Failed"}
	, { id = GameData.Sound.APOTHECARY_ITEM_REMOVED        , name = "Apothecary Item Removed"}
	, { id = GameData.Sound.APOTHECARY_RESOURCE_ADDED      , name = "Apothecary Resource Added"}
	, { id = GameData.Sound.BETA_WARNING                   , name = "Beta Warning"}
	, { id = GameData.Sound.BUTTON_CLICK                   , name = "Button Click"}
	, { id = GameData.Sound.BUTTON_OVER                    , name = "Button Over"}
	, { id = GameData.Sound.CULTIVATING_HARVEST_CROP       , name = "Cultivating Harvest Crop"}
	, { id = GameData.Sound.CULTIVATING_NUTRIENT_ADDED     , name = "Cultivating Nutrient Added"}
	, { id = GameData.Sound.CULTIVATING_SEED_ADDED         , name = "Cultivating Seed Added"}
	, { id = GameData.Sound.CULTIVATING_SOIL_ADDED         , name = "Cultivating Soil Added"}
	, { id = GameData.Sound.CULTIVATING_WATER_ADDED        , name = "Cultivating Water Added"}
	, { id = GameData.Sound.ICON_CLEAR                     , name = "Icon Clear"}
	, { id = GameData.Sound.ICON_DROP                      , name = "Icon Drop"}
	, { id = GameData.Sound.ICON_PICKUP                    , name = "Icon Pickup"}
	, { id = GameData.Sound.LOOT_MONEY                     , name = "Loot Money"}
	, { id = GameData.Sound.MONETARY_TRANSACTION           , name = "Monetary Transaction"}
	, { id = GameData.Sound.OBJECTIVE_CAPTURE              , name = "Objective Capture"}
	, { id = GameData.Sound.OBJECTIVE_LOSE                 , name = "Objective Lose"}
	, { id = GameData.Sound.PREGAME_PLAY_GAME_BUTTON       , name = "Pregame Play Game Button"}
	, { id = GameData.Sound.PUBLIC_TOME_UNLOCKED           , name = "Public Tome Unlocked"}
	, { id = GameData.Sound.QUEST_ABANDONED                , name = "Quest Abandoned"}
	, { id = GameData.Sound.QUEST_ACCEPTED                 , name = "Quest Accepted"}
	, { id = GameData.Sound.QUEST_COMPLETED                , name = "Quest Completed"}
	, { id = GameData.Sound.QUEST_OBJECTIVES_COMPLETED     , name = "Quest Objective Complete"}
	, { id = GameData.Sound.RESPAWN                        , name = "Respawn"}
	, { id = GameData.Sound.RVR_FLAG_OFF                   , name = "RvR Flag Off"}
	, { id = GameData.Sound.RVR_FLAG_ON                    , name = "RvR Flag On"}
	, { id = GameData.Sound.TARGET_DESELECT                , name = "Target Deselect"}
	, { id = GameData.Sound.TARGET_SELECT                  , name = "Target Select"}
	, { id = GameData.Sound.TOME_TURN_PAGE                 , name = "Tome Turn Page"}
	, { id = GameData.Sound.WINDOW_CLOSE                   , name = "Window Close"}
	, { id = GameData.Sound.WINDOW_OPEN                    , name = "Window Open"}
	, { id = GameData.Sound.CLOSE_WORLD_MAP                , name = "World Map Close"}
	, { id = GameData.Sound.OPEN_WORLD_MAP                 , name = "World Map Open"}	          
	                          }
	
