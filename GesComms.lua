-- Ges = {}
if Ges.Comms == nil then
	Ges.Comms = {}
	Ges.Comms.AreaID     = 0
	Ges.Comms.CastStatus = 0
	Ges.Comms.Channel = {}
	Ges.Comms.Channel.Cmd = ""
	Ges.Comms.Channel.Id  = nil
	Ges.Comms.Channel.LogName = ""
	Ges.Comms.Channel.Colour = { r = 128, g = 0, b = 128 }
	Ges.Comms.Channel.Join  = 0
	Ges.Comms.PlayerName = string.match(tostring(GameData.Player.name),"%a+")
	Ges.Comms.QueueSend = {}
	Ges.Comms.QueueAnnounce = {}
	Ges.Comms.Sending    = false
	Ges.Comms.SendingKey = 0
	Ges.Comms.SendId     = 0
	Ges.Comms.Sent       = false
	Ges.Comms.SendTime     = 0
	Ges.Comms.AnnounceTime = 0
	Ges.Comms.TotalTime    = 0
	Ges.Comms.Channels = { [1] = "KageOrder" , [2] = "KageDestro" }
end

-----------------------------------
-- Inter communication functions --
-----------------------------------
function Ges.CommsInitCompelte()
	Ges.DebugMsg(L"Entering CommsInitCompelte",3)
	Ges.CommsFindChannel(true)
	Ges.DebugMsg(L"Leaving CommsInitCompelte",3)
end

function Ges.CommsFindChannel(reset)
	-- Check to see if the channel has already been found
	if Ges.Comms.Channel.Id == nil or reset then
		-- Locate which channel we have been assigned to
		for k, a in pairs(ChatSettings.Channels) do
			if a ~= nil and a.id ~= nil and a.serverCmd ~= nil and a.labelText ~= nil and a.logName ~= nil then
				Ges.DebugMsg(L"CFC: Id ["..a.id..L"] ServerCmd ["..a.serverCmd..L"] LogName ["..towstring(a.logName)..L"] LabelText ["..a.labelText..L"]", 2)
				local label = tostring(a.labelText)
				if label:match(Ges.Comms.Channels[GameData.Player.realm]) then
					Ges.Comms.Channel.Cmd = tostring(a.serverCmd)
					Ges.Comms.Channel.Id = a.id
					Ges.Comms.Channel.LogName = a.logName
					ChatSettings.ChannelColors[Ges.Comms.Channel.Id] = Ges.Comms.Channel.Colour
					Ges.CommsHideChannel()
					Ges.DebugMsg(L"Leaving CommsFindChannel - Channel found",3)
					return Ges.Comms.Channel.Cmd
				end
			end
		end
		-- Chennel was not found, so join it
		Ges.DebugMsg(L"CommsFindChannel: Channel not found", 0)
		Ges.CommsJoinChannel()
		return nil
	end
	return Ges.Comms.Channel.Cmd
end

function Ges.CommsJoinChannel()
	-- Ges.DebugMsg(L"Entering CommsJoinChannel",3)
		-- if Ges.Comms.Channel.Join > 5 then
		-- The addon has tried to join the channel more than 5 times, something is wrong, so shut down and stop draining resources.
			-- EA_ChatWindow.Print(L"Ges: Unable to join channel ["..towstring(Ges.Comms.Channels[GameData.Player.realm])..L"] after 5 attempts. Shutting down.")
		-- Ges.Shutdown()
	-- end
	Ges.Comms.Channel.Join = Ges.Comms.Channel.Join + 1
	SendChatText(L"/channeljoin "..towstring(Ges.Comms.Channels[GameData.Player.realm]),L"")
--	SystemData.UserInput.ChatText = L"/channeljoin "..towstring(Ges.Comms.Channels[GameData.Player.realm])
--	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
	Ges.DebugMsg(L"Leaving CommsJoinChannel",3)
end

function Ges.CommsHideChannel()
	Ges.DebugMsg(L"Entering CommsHideChannel",3)
	-- Auto filter out the comms channel from all other tabs aside from the testing tab.
	for k, a in pairs(EA_ChatWindowGroups) do
		if a ~= nil and a.Tabs ~= nil then
			for g, e in pairs(a.Tabs) do
				local windowName = EA_ChatTabManager.GetTabName(e.tabManagerId).."TextLog"
				if e.tabText == L"Kage" then
					LogDisplaySetFilterState(windowName, Ges.Comms.Channel.LogName, Ges.Comms.Channel.Id, true)
					LogDisplaySetFilterColor(windowName, Ges.Comms.Channel.LogName, Ges.Comms.Channel.Id, Ges.Comms.Channel.Colour.r, Ges.Comms.Channel.Colour.g, Ges.Comms.Channel.Colour.b)
					--[[  
						TODO: Look at using diffrent Channel Id's to hide Join/Leave messages.  
						LogDisplaySetFilterColor( tabLogDisplayName, "System", SystemData.SystemLogFilters.GENERAL, DefaultColor.CLEAR_WHITE.r,DefaultColor.CLEAR_WHITE.g, DefaultColor.CLEAR_WHITE.b );
						LogDisplaySetFilterColor( tabLogDisplayName, "System", SystemData.SystemLogFilters.NOTICE , DefaultColor.YELLOW.r, DefaultColor.YELLOW.g, DefaultColor.YELLOW.b );
						LogDisplaySetFilterColor( tabLogDisplayName, "System", SystemData.SystemLogFilters.ERROR  , DefaultColor.ORANGE.r, DefaultColor.ORANGE.g, DefaultColor.ORANGE.b );
						TODO: Investigate the potential for different tab windows not being handled by this code.
					--]]
					
				else
					LogDisplaySetFilterState(windowName, Ges.Comms.Channel.LogName, Ges.Comms.Channel.Id, false)
				end
				-- Hide the Join/Leave channel messages.
				LogDisplaySetFilterState(windowName, "Chat", 6, false)
			end
		end
	end
	LogDisplaySetFilterState( "SiegeWeaponGeneralFireWindowChatLogDisplay", Ges.Comms.Channel.LogName, Ges.Comms.Channel.Id, false )
	Ges.DebugMsg(L"Leaving CommsHideChannel",3)
end

function Ges.CommsCreateDetails(status, state)
	Ges.DebugMsg(L"Entering CommsCreateDetails",3)
	local guardDetails = {
		  [GesConstants.COMMS_DATA_ADDON]       = "Ges"
		, [GesConstants.COMMS_DATA_SEND_ID]     = 0
		, [GesConstants.COMMS_DATA_STATUS]      = status
		, [GesConstants.COMMS_DATA_CASTER_ID]   = tostring(GameData.Player.worldObjNum)
		, [GesConstants.COMMS_DATA_CASTER_NAME] = Ges.Comms.PlayerName
		, [GesConstants.COMMS_DATA_TARGET_ID]   = Ges.Target.EntityId
		, [GesConstants.COMMS_DATA_TARGET_NAME] = Ges.Target.Name
		, [GesConstants.COMMS_DATA_STATE]       = state
	                     }
	Ges.DebugMsg(L"Leaving CommsCreateDetails",3)
	return guardDetails
end

function Ges.CommsAddToQueue(commsType, guardDetails)
	Ges.DebugMsg(L"Entering CommsAddToQueue",3)
	if commsType == GesConstants.COMMS_TYPE_SEND then
		Ges.Comms.SendId = Ges.Comms.SendId + 1
		guardDetails[GesConstants.COMMS_DATA_SEND_ID] = Ges.Comms.SendId
		table.insert(Ges.Comms.QueueSend, guardDetails)
	else
		table.insert(Ges.Comms.QueueAnnounce, guardDetails)
	end
	Ges.DebugMsg(L"Leaving CommsAddToQueue",3)
end

function Ges.CommsProcessQueues()
	-- Warhammer API only allows 1 Broadcast Event per focus so only process 1 entry and remove it from the queue.
	if #Ges.Comms.QueueSend > 0 and Ges.Comms.SendTime > GesConstants.COMMS_SEND_THROTTLE then
		-- Check to see if a message has already been sent and needs to be removed.
		if Ges.Comms.Sent then
			if Ges.Comms.QueueSend[Ges.Comms.SendingKey] ~= nil and
			   Ges.Comms.QueueSend[Ges.Comms.SendingKey][GesConstants.COMMS_DATA_SEND_ID] == Ges.Comms.SendId then
				Ges.DebugMsg("CR: Removing Key["..Ges.Comms.SendingKey.."]",1)
				table.remove(Ges.Comms.QueueSend, Ges.Comms.SendingKey)
			end
			Ges.Comms.Sent = false
			Ges.Comms.SendingKey = 0
		end
		-- Get the next message on the Send Queue and send it
		for k, a in ipairs(Ges.Comms.QueueSend) do
			Ges.Comms.Sending = true
			Ges.Comms.Sent = false
			Ges.Comms.SendingKey = k
			Ges.CommsSend(a)
			break
		end
		Ges.Comms.SendTime = 0
	elseif #Ges.Comms.QueueAnnounce > 0 and Ges.Comms.AnnounceTime > GesConstants.COMMS_ANNOUNCE_THROTTLE then
		local key, data
		-- Send a message from the Announce queue
		for k, a in ipairs(Ges.Comms.QueueAnnounce) do
			key = k
			data = a
			break
		end
		Ges.CommsAnnounce(data)
		table.remove(Ges.Comms.QueueAnnounce, key)
		Ges.Comms.AnnounceTime = 0
	end
end

function Ges.CommsSend(sendDetails)
	Ges.DebugMsg(L"Entering CommsSend",3)
	-- If we can't find a channel, exit
	if Ges.CommsFindChannel(false) == nil then return false end
	-- Create the message to be broadcast
	local message = Ges.Comms.Channel.Cmd.." "..sendDetails[GesConstants.COMMS_DATA_ADDON].."|"
	                                          ..sendDetails[GesConstants.COMMS_DATA_SEND_ID].."|"
	                                          ..sendDetails[GesConstants.COMMS_DATA_STATUS].."|"
	                                          ..sendDetails[GesConstants.COMMS_DATA_CASTER_ID].."|"
	                                          ..sendDetails[GesConstants.COMMS_DATA_CASTER_NAME].."|"
	                                          ..sendDetails[GesConstants.COMMS_DATA_TARGET_ID].."|"
	                                          ..sendDetails[GesConstants.COMMS_DATA_TARGET_NAME]
	-- Broadcast the message
	SendChatText(towstring(message),L"")
--	SystemData.UserInput.ChatText = towstring(message)
--	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
	Ges.DebugMsg(L"Leaving CommsSend",3)
end

function Ges.CommsAnnounce(sendDetails)
	Ges.DebugMsg(L"Entering Announce",3)
	local message = ""
	local channel = towstring(GesConstants.Channels[Ges.Settings.Channel].channel)
	local channelID = GesConstants.Channels[Ges.Settings.Channel].id
	if (channel == L"") then
		Ges.DebugMsg("Announce: User has disabled announcements, exiting",3)
		return
	end
	-- Check to see which channel has been selected and set chat to the desired channel
	if (channel == L"sm") then
		Ges.DebugMsg(L"Announce: Smart channel detected",2)
		-- Ascertain users status and set to appropriate channel.
		if (GameData.Player.isInScenario) then
			Ges.DebugMsg(L"Announce: Channel set to /sp",2)
			message = ChatSettings.Channels[SystemData.ChatLogFilters.SCENARIO_GROUPS].serverCmd
		else
			Ges.DebugMsg(L"Announce: Channel set to /p",2)
			message = ChatSettings.Channels[SystemData.ChatLogFilters.GROUP].serverCmd
		end
	elseif channel == L"d" then
		message = ChatSettings.Channels[EA_ChatWindow.curChannel].serverCmd
		if EA_ChatWindow.curChannel == SystemData.ChatLogFilters.TELL_SEND then
			if EA_ChatWindow.whisperTarget ~= nil then
				message = message..L" "..EA_ChatWindow.whisperTarget
			else
				message = ChatSettings.Channels[SystemData.ChatLogFilters.SAY].serverCmd	
			end
		elseif EA_ChatWindow.curChannel == SystemData.ChatLogFilters.ADVICE then
			-- Check to see if user is going to announce on the Advice channel
			-- If so redirect to the /say channel instead
			message = ChatSettings.Channels[SystemData.ChatLogFilters.SAY].serverCmd
		end
	else
		message = ChatSettings.Channels[channelID].serverCmd
	end

	-- Set the text to be sent
	local targetName = towstring(sendDetails[GesConstants.COMMS_DATA_TARGET_NAME])
	if sendDetails[GesConstants.COMMS_DATA_STATUS] == GesConstants.STATUS_APPLIED then
		if sendDetails[GesConstants.COMMS_DATA_STATE] == GesConstants.CAST_FAILED then
			Ges.DebugMsg(L"Announce: Casting Unsuccessful",2)
			message = message..L" Guarding "..targetName..L" was unsuccessful."
		else
			Ges.DebugMsg(L"Announce: Casting Successful",2)
			message = message..L" Guarding "..targetName..L" "..Ges.Settings.Message
		end
	else
		Ges.DebugMsg(L"Announce: Guard has been removed",2)
		message = message..L" Guard has been removed from "..targetName
	end
	-- Send the text
	SendChatText(towstring(message),L"")
	Ges.DebugMsg(L"Leaving Announce",3)
end

function Ges.CommsReceived()
	-- Ges.DebugMsg("CR: Type["..tostring(GameData.ChatData.type).."] Name["..tostring(GameData.ChatData.name).."]", 2)
	-- Don't process the comms unless we have to
	if Ges.CommsFindChannel(false) ~= nil             and
		Ges.Comms.Sending                              and
		Ges.Comms.Channel.Id == GameData.ChatData.type then
		-- Check to see if the comms is ours
		if GameData.ChatData.name == GameData.Player.name then
			-- Check to see if it is the comms we sent and if so then set the triggers so it can be deleted.
			local contents = StringSplit(tostring(GameData.ChatData.text),"|")
			-- Check to see if the contents are specific to the Ges addon as all medikage addons share the same channel
			if contents[GesConstants.COMMS_DATA_ADDON] == "Ges" then
				Ges.DebugMsg("CR: It's a Ges comms. SendId ["..contents[GesConstants.COMMS_DATA_SEND_ID].."]",1)
				Ges.Comms.Sending = false
				Ges.Comms.Sent = true
				Ges.Comms.SendId = tonumber(contents[GesConstants.COMMS_DATA_SEND_ID])
			end
		end
	end
end

function Ges.CommsUpdate(timePassed)
	Ges.Comms.TotalTime = Ges.Comms.TotalTime + timePassed
	Ges.Comms.SendTime = Ges.Comms.SendTime + timePassed
	Ges.Comms.AnnounceTime = Ges.Comms.AnnounceTime + timePassed

	if Ges.Comms.TotalTime > GesConstants.COMMS_THROTTLE then
		Ges.CommsProcessQueues()
		Ges.Comms.TotalTime = 0
	end
end
