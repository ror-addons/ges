<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Ges" version="1.3" date="17/08/2010" >
		<Author name="Medikage" email="medikage@gmail.com" />
		<Description text="The Ges addon, Enables you to locate your guard target (or Guardee) easily and tracks their health for you.  Never lose your target in a pitched battle ever again."/>		
		<VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibSlash" />
			<Dependency name="EA_ChatWindow" />
			<Dependency name="EASystem_Utils"/>
			<Dependency name="EASystem_WindowUtils"/>
		</Dependencies>
		<Files>
			<File name="GesConstants.lua" />
			<File name="Ges.xml" />
			<File name="GesOptions.xml" />
			<File name="GesComms.lua" />
		</Files>
		<SavedVariables>
			<SavedVariable name="Ges.Settings" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="Ges.Init" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="Ges.OnUpdate" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="Ges.Shutdown" />
		</OnShutdown>
		<WARInfo>
			<Categories>
			    <!-- Whilst I realise these are wrong, there is a current bug
					 within warhammer that puts them all out by 1, so in game, 
					 these categories actually align properly. -->
				<Category name="MAIL"/> 
				<Category name="COMBAT"/>
				<Category name="ACTION_BARS" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="IRON_BREAKER" />
				<Career name="BLACK_ORC" />
				<Career name="KNIGHT" />
				<Career name="CHOSEN" />
				<Career name="SWORDMASTER" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>


