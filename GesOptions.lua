local EnableModCheckToggle           = false
local EnableButtonCheckToggle        = false
local EnableDebugCheckToggle         = false
local EnableMonitorIconCheckToggle   = false
local EnableMonitorHealthCheckToggle = false
local EnableMonitorClickCheckToggle  = false
local EnableTrackerIconCheckToggle   = false
local EnableTrackerHealthCheckToggle = false
local EnableExtraFeatureCheckToggle  = false
local EnableSoundCheckToggle         = false

local function assertNumber(str)
	local val = tonumber(str)
	if type(val) ~= "number" then return 0 end
	return val
end

-- Ges = {}
if Ges.Options == nil then
	Ges.Options = {}
	Ges.Options.Window  = "GesOptions"

	Ges.Options.CurrentTab  = 1
	Ges.Options.Tabs = {
	   [GesConstants.TAB_GENERAL_ID] = { name = Ges.Options.Window.."TabsGeneral" , label = L"General" , altered = false }
	,  [GesConstants.TAB_COLOUR_ID]  = { name = Ges.Options.Window.."TabsColour"  , label = L"Colours" , altered = false }
	,  [GesConstants.TAB_EXTRAS_ID]  = { name = Ges.Options.Window.."TabsExtras"  , label = L"Extras"  , altered = false }
	,  [GesConstants.TAB_SOUNDS_ID]  = { name = Ges.Options.Window.."TabsSounds"  , label = L"Sounds"  , altered = false }
	                   }

	Ges.Options.General = {}
	Ges.Options.General.Window = Ges.Options.Window.."General"
	Ges.Options.General.Labels = {
	   { name = Ges.Options.General.Window.."ModLabel"           , value = "Enable the addon"}
	 , { name = Ges.Options.General.Window.."ToggleLabel"        , value = "Show the toggle button"}
	 , { name = Ges.Options.General.Window.."ToggleColourLabel"  , value = "Toggle button colour"}
	 , { name = Ges.Options.General.Window.."ChannelsLabel"      , value = "Select a channel to announce on"}
	 , { name = Ges.Options.General.Window.."MessageLabel"       , value = "Type a custom message"}
	 , { name = Ges.Options.General.Window.."SoundEnableLabel"   , value = "Enable sounds"}
	 , { name = Ges.Options.General.Window.."MonitorLabel"       , value = "Monitor Icon Options:"}
	 , { name = Ges.Options.General.Window.."MonitorIconLabel"   , value = "Enable the icon"}
	 , { name = Ges.Options.General.Window.."MonitorHealthLabel" , value = "Enable health tracking"}
	 , { name = Ges.Options.General.Window.."MonitorClickLabel"  , value = "Enable click through"}
	 , { name = Ges.Options.General.Window.."TrackerLabel"       , value = "Tracker Icon Options:"}
	 , { name = Ges.Options.General.Window.."TrackerIconLabel"   , value = "Enable the icon"}
	 , { name = Ges.Options.General.Window.."TrackerHealthLabel" , value = "Enable health tracking"}
	                             }

	Ges.Options.Colour = {}
	Ges.Options.Colour.Window = Ges.Options.Window.."Colour"
	Ges.Options.Colour.Labels = {
	   { name = Ges.Options.Colour.Window.."IconSelectionLabel"    , value = "Select which icon to alter:"}
	 , { name = Ges.Options.Colour.Window.."DefaultColourLabel"    , value = "Is alive"}
	 , { name = Ges.Options.Colour.Window.."OfflineColourLabel"    , value = "Is disconnected"}
	 , { name = Ges.Options.Colour.Window.."DeathColourLabel"      , value = "Is dead"}
	 , { name = Ges.Options.Colour.Window.."HealthSectionLabel"    , value = "Colour to display when target:"}
	 , { name = Ges.Options.Colour.Window.."FullHealthColourLabel" , value = "Health is full"}
	 , { name = Ges.Options.Colour.Window.."HighHealthColourLabel" , value = "Health is high"}
	 , { name = Ges.Options.Colour.Window.."MedHealthColourLabel"  , value = "Health is medium"}
	 , { name = Ges.Options.Colour.Window.."MinHealthColourLabel"  , value = "Health is low"}
	 , { name = Ges.Options.Colour.Window.."HighHealthLabel"       , value = "High health threshold"}
	 , { name = Ges.Options.Colour.Window.."MedHealthLabel"        , value = "Medium health threshold"}
	 , { name = Ges.Options.Colour.Window.."MinHealthLabel"        , value = "Low health threshold"}
	                           }

	Ges.Options.Extras = {}
	Ges.Options.Extras.Window = Ges.Options.Window.."Extras"
	Ges.Options.Extras.Labels = {
	  { name = Ges.Options.Extras.Window.."FeatureSectionLabel"   , value = "Add extra features to the icons:"}
	, { name = Ges.Options.Extras.Window.."IconSelectionLabel"    , value = "Select which icon to add to:"}
	, { name = Ges.Options.Extras.Window.."FeatureSelectionLabel" , value = "Select which feature to add:"}
	, { name = Ges.Options.Extras.Window.."FeatureEnableLabel"    , value = "Enable the feature"}
	, { name = Ges.Options.Extras.Window.."FeaturePositionLabel"  , value = "Select feature's base position:"}
	, { name = Ges.Options.Extras.Window.."FeatureOffsetLabel"    , value = "Fine tune feature's position:"}
	, { name = Ges.Options.Extras.Window.."FeatureOffsetXLabel"   , value = "X:"}
	, { name = Ges.Options.Extras.Window.."FeatureOffsetYLabel"   , value = "Y:"}
	, { name = Ges.Options.Extras.Window.."TrackerOffsetLabel"    , value = "Adjust Tracker icon position:"}
	, { name = Ges.Options.Extras.Window.."TrackerOffsetXLabel"   , value = "X:"}
	, { name = Ges.Options.Extras.Window.."TrackerOffsetYLabel"   , value = "Y:"}
	                            }

	Ges.Options.Sounds = {}
	Ges.Options.Sounds.Window = Ges.Options.Window.."Sounds"
	Ges.Options.Sounds.Labels = {
	  { name = Ges.Options.Sounds.Window.."AppliedLabel"   , value = "Guard is applied"}
	, { name = Ges.Options.Sounds.Window.."RemovedLabel"   , value = "Guard is removed"}
	, { name = Ges.Options.Sounds.Window.."TitleLabel"     , value = "Play a sound once when your target's health falls below the set threshold"}
	, { name = Ges.Options.Sounds.Window.."TypeLabel"      , value = "Select a sound"}
	, { name = Ges.Options.Sounds.Window.."ThresholdLabel" , value = "Threshold"}
	, { name = Ges.Options.Sounds.Window.."ButtonLabel"    , value = "Listen"}
	                            }
end

-------------------------------------------------
---------- Window Specific Functions ------------
-------------------------------------------------

------------------------------
--  Init Options Functions  --
------------------------------
function Ges.OptionsWindowInit()
	Ges.DebugMsg(L"Entering OptionsWindowInit",3)
	-- Create and register the options window and then hide it.
	CreateWindow(Ges.Options.Window, true)
	WindowSetShowing(Ges.Options.Window,false)
	Ges.DebugMsg(L"OptionsWindowInit: Window created",3)
	-- Set Title Bar text
	LabelSetText(Ges.Options.Window.."TitleBarText", L"Guard Enhancement System Options v"..towstring(Ges.Settings.Version))
	-- Set Tab text
	for i, v in ipairs(Ges.Options.Tabs) do
		ButtonSetText(v.name, v.label)
	end
	-- Set Button labels
	ButtonSetText(Ges.Options.Window.."SaveButton" , L"Save")
	ButtonSetText(Ges.Options.Window.."CloseButton", L"Close")
	-- Initialise tha tab windows
	Ges.OptionsInitGeneral()
	Ges.OptionsInitColour()
	Ges.OptionsInitExtras()
	Ges.OptionsInitSounds()
	Ges.Options.CurrentTab = GesConstants.TAB_GENERAL_ID
	Ges.DebugMsg(L"Leaving OptionsWindowInit",3)
end

function Ges.OptionsInitGeneral()
	Ges.DebugMsg(L"Entering OptionsInitGeneral",3)
	-- Hide the General Tab Window until it is selected
	WindowSetShowing(Ges.Options.General.Window,false)
	-- Set the text in all of the labels for the General Tab Window
	for k, a in ipairs(Ges.Options.General.Labels) do
		LabelSetText(a.name,towstring(a.value))
	end
	--Pre-populate the combo boxes with the available icon colours
	for k, a in ipairs(GesConstants.IconsBackgrounds) do
		ComboBoxAddMenuItem(Ges.Options.General.Window.."ToggleColourComboBox", towstring(a.name))
	end
	--Pre-populate the combo boxes with the available channels
	for k, a in pairs(GesConstants.Channels) do
		ComboBoxAddMenuItem(Ges.Options.General.Window.."ChannelsComboBox", towstring(a.desc))
	end
	Ges.DebugMsg(L"Leaving OptionsInitGeneral",3)
end

function Ges.OptionsInitColour()
	Ges.DebugMsg(L"Entering OptionsInitColour",3)
	-- Hide the Colour Tab Window until it is selected
	WindowSetShowing(Ges.Options.Colour.Window,false)
	-- Set the text in all of the labels for the Colour Tab Window
	for k, a in ipairs(Ges.Options.Colour.Labels) do
		LabelSetText(a.name,towstring(a.value))
	end
	-- Pre-populate the combo box with the various icon types
	for k, a in ipairs(GesConstants.Icons) do
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."IconSelectionComboBox", towstring(a.label))
	end
	-- Pre-populate the combo boxes with the available colours
	for k, a in ipairs(GesConstants.IconsBackgrounds) do
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."DefaultColourComboBox"   , towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."OfflineColourComboBox"   , towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."DeathColourComboBox"     , towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."FullHealthColourComboBox", towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."HighHealthColourComboBox", towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."MedHealthColourComboBox" , towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Colour.Window.."MinHealthColourComboBox" , towstring(a.name))
	end
	Ges.DebugMsg(L"Leaving OptionsInitColour",3)
end

function Ges.OptionsInitExtras()
	Ges.DebugMsg(L"Entering OptionsInitExtras",3)
	-- Hide the Extras Tab Window until it is selected
	WindowSetShowing(Ges.Options.Extras.Window,false)
	-- Set the text in all of the labels for the Extras Tab Window
	for k, a in ipairs(Ges.Options.Extras.Labels) do
		LabelSetText(a.name,towstring(a.value))
	end
	-- Pre-populate the combo box with the various icon types
	for k, a in ipairs(GesConstants.Icons) do
		ComboBoxAddMenuItem(Ges.Options.Extras.Window.."IconSelectionComboBox", towstring(a.label))
	end
	-- Pre-populate the combo box with the various feature types
	for k, a in ipairs(GesConstants.Features) do
		ComboBoxAddMenuItem(Ges.Options.Extras.Window.."FeatureSelectionComboBox",towstring(a.name))
	end
	-- Pre-populate the combo box with the various position anchors types
	for k, a in ipairs(GesConstants.Anchors) do
		ComboBoxAddMenuItem(Ges.Options.Extras.Window.."FeaturePositionComboBox", towstring(a.name))
	end
	Ges.DebugMsg(L"Leaving OptionsInitExtras",3)
end

function Ges.OptionsInitSounds()
	Ges.DebugMsg(L"Entering OptionsInitSounds",3)
	-- Hide the Sounds Tab Window until it is selected
	WindowSetShowing(Ges.Options.Sounds.Window,false)
	-- Set the text in all of the labels for the Sounds Tab Window
	for k, a in ipairs(Ges.Options.Sounds.Labels) do
		LabelSetText(a.name,towstring(a.value))
	end
	-- Pre-populate the combo box with the various sound types
	for k, a in ipairs(GesConstants.SoundTypes) do
		ComboBoxAddMenuItem(Ges.Options.Sounds.Window.."AppliedComboBox", towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Sounds.Window.."RemovedComboBox", towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Sounds.Window.."TypeComboBox1"  , towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Sounds.Window.."TypeComboBox2"  , towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Sounds.Window.."TypeComboBox3"  , towstring(a.name))
		ComboBoxAddMenuItem(Ges.Options.Sounds.Window.."TypeComboBox4"  , towstring(a.name))
	end
	Ges.DebugMsg(L"Leaving OptionsInitSounds",3)
end
---------------------------------
--   Show Settings functions   --
---------------------------------

function Ges.OptionsShow(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering OptionsShow",3)
	-- Ensure that all of the visual settings equal the stored settings.
	Ges.OptionsShowGeneral()	
	Ges.OptionsShowColour()
	Ges.OptionsShowExtras()
	Ges.OptionsShowSounds()
	-- Check if the General Tab is showing and if not, show it.
	if Ges.Options.CurrentTab ~= GesConstants.TAB_GENERAL_ID then
		Ges.OptionsHideTab(Ges.Options.CurrentTab)
		Ges.Options.CurrentTab = GesConstants.TAB_GENERAL_ID
	end
	Ges.OptionsShowTab(Ges.Options.CurrentTab)
	-- Now that all of the settings have been set, show the Options window.
	WindowSetShowing(Ges.Options.Window,true)
	Ges.DebugMsg(L"Leaving OptionsShow",3)
end

function Ges.OptionsShowTab(tabID)
	Ges.DebugMsg("Entering OptionsShowTab("..tabID..")",3)
	-- Loop through the tab buttons and set their pressed flag.
	for i, v in ipairs(Ges.Options.Tabs) do
		ButtonSetPressedFlag( v.name, i == tabID )
   end
	-- Check which tab needs to be shown and call the appropriate function
	if tabID == GesConstants.TAB_GENERAL_ID then
		WindowSetShowing(Ges.Options.General.Window,true)
	elseif tabID == GesConstants.TAB_COLOUR_ID then
		WindowSetShowing(Ges.Options.Colour.Window,true)
	elseif tabID == GesConstants.TAB_EXTRAS_ID then
		WindowSetShowing(Ges.Options.Extras.Window,true)
		Ges.OptionsExtrasReset()
	elseif tabID == GesConstants.TAB_SOUNDS_ID then
		WindowSetShowing(Ges.Options.Sounds.Window,true)
	end
	Ges.DebugMsg("Leaving OptionsShowTab("..tabID..")",3)
end

function Ges.OptionsHideTab(tabID)
	Ges.DebugMsg("Entering OptionsHideTab("..tabID..")",3)
	-- Check which tab window needs hiding and hide it.
	if tabID == GesConstants.TAB_GENERAL_ID then
		WindowSetShowing(Ges.Options.General.Window,false)
	elseif tabID == GesConstants.TAB_COLOUR_ID then
		WindowSetShowing(Ges.Options.Colour.Window,false)
	elseif tabID == GesConstants.TAB_EXTRAS_ID then
		WindowSetShowing(Ges.Options.Extras.Window,false)
		Ges.OptionsExtrasReset()
	elseif tabID == GesConstants.TAB_SOUNDS_ID then
		WindowSetShowing(Ges.Options.Sounds.Window,false)
	end
	Ges.DebugMsg("Leaving OptionsHideTab("..tabID..")",3)
end

function Ges.OptionsShowGeneral()
	Ges.DebugMsg(L"Entering OptionsShowGeneral",3)
	-- Enable Addon CheckBox Setting stored in Ges.Enabled
	EnableModCheckToggle = Ges.Settings.Enabled
	ButtonSetPressedFlag(Ges.Options.General.Window.."ModCheckBox",EnableModCheckToggle)
	-- Enable Toggle Button CheckBox Setting stored in Ges.Settings.ShowButton
	EnableButtonCheckToggle = Ges.Settings.ShowButton
	ButtonSetPressedFlag(Ges.Options.General.Window.."ToggleCheckBox",EnableButtonCheckToggle)
	-- Enable Sounds CheckBox Setting stored in Ges.Settings.Sounds.Enabled
	EnableSoundCheckToggle = Ges.Settings.Sounds.Enabled
	ButtonSetPressedFlag(Ges.Options.General.Window.."SoundEnableCheckBox",EnableSoundCheckToggle)
	--
	ComboBoxSetSelectedMenuItem(Ges.Options.General.Window.."ToggleColourComboBox",Ges.Settings.ToggleColour)
	ComboBoxSetSelectedMenuItem(Ges.Options.General.Window.."ChannelsComboBox", Ges.Settings.Channel)
	TextEditBoxSetText(Ges.Options.General.Window.."MessageEditBox", Ges.Settings.Message)
	--
	EnableMonitorIconCheckToggle = Ges.Settings.Monitor.Enabled
	ButtonSetPressedFlag(Ges.Options.General.Window.."MonitorIconCheckBox",EnableMonitorIconCheckToggle)
	EnableMonitorHealthCheckToggle = Ges.Settings.Monitor.Health.Enabled
	ButtonSetPressedFlag(Ges.Options.General.Window.."MonitorHealthCheckBox",EnableMonitorHealthCheckToggle)
	EnableMonitorClickCheckToggle = not Ges.Settings.Monitor.Movable
	ButtonSetPressedFlag(Ges.Options.General.Window.."MonitorClickCheckBox",EnableMonitorClickCheckToggle )
	--
	EnableTrackerIconCheckToggle = Ges.Settings.Tracker.Enabled
	ButtonSetPressedFlag(Ges.Options.General.Window.."TrackerIconCheckBox",EnableTrackerIconCheckToggle)
	EnableTrackerHealthCheckToggle = Ges.Settings.Tracker.Health.Enabled
	ButtonSetPressedFlag(Ges.Options.General.Window.."TrackerHealthCheckBox",EnableTrackerHealthCheckToggle)
	Ges.DebugMsg(L"Leaving OptionsShowGeneral",3)
end

function Ges.OptionsShowColour()
	Ges.DebugMsg(L"Entering OptionsShowColour",3)
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."IconSelectionComboBox", GesConstants.ICON_MONITOR)
	Ges.OptionsShowColourMonitor()
	Ges.DebugMsg(L"Leaving OptionsShowColour",3)
end

function Ges.OptionsShowColourMonitor()
	Ges.DebugMsg(L"Entering OptionsShowColourMonitor",3)
	-- Populate the values for the Colour Tab with the Monitor values stored in the users settings.
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."DefaultColourComboBox"   , Ges.Settings.Monitor.DefaultIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."OfflineColourComboBox", Ges.Settings.Monitor.isOfflineIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."DeathColourComboBox"     , Ges.Settings.Monitor.DeadIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."FullHealthColourComboBox", Ges.Settings.Monitor.Health.FullIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."HighHealthColourComboBox", Ges.Settings.Monitor.Health.HighIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."MedHealthColourComboBox" , Ges.Settings.Monitor.Health.MedIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."MinHealthColourComboBox" , Ges.Settings.Monitor.Health.MinIcon )
	TextEditBoxSetText(Ges.Options.Colour.Window.."HighHealthEditBox", towstring(Ges.Settings.Monitor.Health.HighPerc))
	TextEditBoxSetText(Ges.Options.Colour.Window.."MedHealthEditBox" , towstring(Ges.Settings.Monitor.Health.MedPerc))
	TextEditBoxSetText(Ges.Options.Colour.Window.."MinHealthEditBox" , towstring(Ges.Settings.Monitor.Health.MinPerc))
	Ges.DebugMsg(L"Leaving OptionsShowColourMonitor",3)
end

function Ges.OptionsShowColourTracker()
	Ges.DebugMsg(L"Entering OptionsShowTracker",3)
	-- Populate the values for the Colour Tab with the Tracker values stored in the users settings.
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."DefaultColourComboBox"   , Ges.Settings.Tracker.DefaultIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."OfflineColourComboBox", Ges.Settings.Tracker.isOfflineIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."DeathColourComboBox"     , Ges.Settings.Tracker.DeadIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."FullHealthColourComboBox", Ges.Settings.Tracker.Health.FullIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."HighHealthColourComboBox", Ges.Settings.Tracker.Health.HighIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."MedHealthColourComboBox" , Ges.Settings.Tracker.Health.MedIcon )
	ComboBoxSetSelectedMenuItem(Ges.Options.Colour.Window.."MinHealthColourComboBox" , Ges.Settings.Tracker.Health.MinIcon )
	TextEditBoxSetText(Ges.Options.Colour.Window.."HighHealthEditBox", towstring(Ges.Settings.Tracker.Health.HighPerc))
	TextEditBoxSetText(Ges.Options.Colour.Window.."MedHealthEditBox" , towstring(Ges.Settings.Tracker.Health.MedPerc))
	TextEditBoxSetText(Ges.Options.Colour.Window.."MinHealthEditBox" , towstring(Ges.Settings.Tracker.Health.MinPerc))
	Ges.DebugMsg(L"Leaving OptionsShowTracker",3)
end

function Ges.OptionsShowExtras()
	Ges.DebugMsg(L"Entering OptionsShowExtras",3)
	-- Populate Extra Feature selection
	ComboBoxSetSelectedMenuItem(Ges.Options.Extras.Window.."IconSelectionComboBox", GesConstants.ICON_MONITOR)
	ComboBoxSetSelectedMenuItem(Ges.Options.Extras.Window.."FeatureSelectionComboBox", GesConstants.EXTRAS_TARGET_HEALTH)
	Ges.ExtrasFeatureSelected( GesConstants.EXTRAS_TARGET_HEALTH )
	-- Populate Tracker Offset settings
	TextEditBoxSetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox", towstring(Ges.Settings.Tracker.OffsetX))
	TextEditBoxSetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox", towstring(Ges.Settings.Tracker.OffsetY))
	Ges.DebugMsg(L"Leaving OptionsShowExtras",3)
end

function Ges.OptionsShowSounds()
	Ges.DebugMsg(L"Entering OptionsShowSounds",3)
	-- Populate Sound settings	
	ComboBoxSetSelectedMenuItem(Ges.Options.Sounds.Window.."AppliedComboBox", Ges.Settings.Sounds.Applied)
	ComboBoxSetSelectedMenuItem(Ges.Options.Sounds.Window.."RemovedComboBox", Ges.Settings.Sounds.Removed)
	for k, a in ipairs(Ges.Settings.Sounds.Preferences) do
		ComboBoxSetSelectedMenuItem(Ges.Options.Sounds.Window.."TypeComboBox"..k , a.selection)
		TextEditBoxSetText(Ges.Options.Sounds.Window.."ThresholdEditBox"..k      , towstring(a.threshold))
	end
	Ges.DebugMsg(L"Leaving OptionsShowSounds",3)
end

---------------------------------
--  GUI Interaction functions  --
---------------------------------

function Ges.OptionsSelectTab(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering OptionsSelectTab",3)
	local selectedTab = WindowGetId(SystemData.ActiveWindow.name)
	Ges.DebugMsg("OptionsSelectTab: SelectedTabID ["..selectedTab.."]",2)
	Ges.DebugMsg(L"OptionsSelectTab: CurrentTabID ["..Ges.Options.CurrentTab..L"]",2)
	-- Hide the deselected tab abd show the selected one
	Ges.OptionsHideTab(Ges.Options.CurrentTab)
	Ges.OptionsShowTab(selectedTab)
	-- Store which tab has been selected
	Ges.Options.CurrentTab = selectedTab
	Ges.DebugMsg(L"Leaving OptionsSelectTab",3)
end

function Ges.ColourTabIconSelected( selectionIndex )
	Ges.DebugMsg(L"Entering ColourTabIconSelected",3)
	if selectionIndex == GesConstants.ICON_MONITOR then
		Ges.OptionsShowColourMonitor()
	elseif selectionIndex == GesConstants.ICON_TRACKER then
		Ges.OptionsShowColourTracker()
	else
		Ges.DebugMsg(L"ColourTabIconSelected: Invalid Selection",0)
	end
	Ges.DebugMsg(L"Leaving ColourTabIconSelected",3)
end

function Ges.ExtrasIconSelected( selectionIndex )
	Ges.DebugMsg(L"Entering ExtrasIconSelected",3)
	Ges.ExtrasFeatureSelected(ComboBoxGetSelectedMenuItem(Ges.Options.Extras.Window.."FeatureSelectionComboBox"))
	Ges.DebugMsg(L"Leaving ExtrasIconSelected",3)
end

function Ges.ExtrasFeatureSelected( selectionIndex )
	Ges.DebugMsg(L"Entering ExtrasFeatureSelected",3)
	local iconSelected = ComboBoxGetSelectedMenuItem(Ges.Options.Extras.Window.."IconSelectionComboBox")
	local featureSelected = nil

	if iconSelected == GesConstants.ICON_MONITOR then
		featureSelected = Ges.Monitor.Extras[selectionIndex]
	elseif iconSelected == GesConstants.ICON_TRACKER then
		featureSelected = Ges.Tracker.Extras[selectionIndex]
	else
		Ges.DebugMsg(L"ExtrasFeatureSelected: Invalid Selection",0)
	end

	EnableExtraFeatureCheckToggle = featureSelected.enabled
	ButtonSetPressedFlag(Ges.Options.Extras.Window.."FeatureEnableCheckBox",EnableExtraFeatureCheckToggle)
	ComboBoxSetSelectedMenuItem( Ges.Options.Extras.Window.."FeaturePositionComboBox"
	                           , featureSelected.anchor
	                           )
	TextEditBoxSetText(Ges.Options.Extras.Window.."FeatureOffsetXEditBox", towstring(featureSelected.offsetX))
	TextEditBoxSetText(Ges.Options.Extras.Window.."FeatureOffsetYEditBox", towstring(featureSelected.offsetY))
	Ges.DebugMsg(L"Leaving ExtrasFeatureSelected",3)
end

function Ges.ExtrasPositionSelected( selectionIndex )
	Ges.DebugMsg(L"Entering ExtrasPositionSelected",3)
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving ExtrasPositionSelected",3)
end

function Ges.ExtrasFeatureOffsetUp(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasFeatureOffsetUp",3)
	local OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."FeatureOffsetYEditBox"))) - 1
	TextEditBoxSetText(Ges.Options.Extras.Window.."FeatureOffsetYEditBox", towstring(OffsetY))
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetUp",3)
end

function Ges.ExtrasFeatureOffsetDown(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasFeatureOffsetDown",3)
	local OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."FeatureOffsetYEditBox"))) + 1
	TextEditBoxSetText(Ges.Options.Extras.Window.."FeatureOffsetYEditBox", towstring(OffsetY))
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving ExtrasFeatureOffsetDown",3)
end

function Ges.ExtrasFeatureOffsetLeft(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasFeatureOffsetLeft",3)
	local OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."FeatureOffsetXEditBox"))) - 1
	TextEditBoxSetText(Ges.Options.Extras.Window.."FeatureOffsetXEditBox", towstring(OffsetX))
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving ExtrasFeatureOffsetLeft",3)
end

function Ges.ExtrasFeatureOffsetRight(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasFeatureOffsetRight",3)
	local OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."FeatureOffsetXEditBox"))) + 1
	TextEditBoxSetText(Ges.Options.Extras.Window.."FeatureOffsetXEditBox", towstring(OffsetX))
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving ExtrasFeatureOffsetRight",3)
end

function Ges.ExtrasFeatureOffsetYKeyEntry(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasTrackerOffsetYKeyEntry",3)
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetYKeyEntry",3)
end

function Ges.ExtrasFeatureOffsetXKeyEntry(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasFeatureOffsetXKeyEntry",3)
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving ExtrasFeatureOffsetXKeyEntry",3)
end

function Ges.ExtrasTrackerOffsetUp(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasTrackerOffsetUp",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox")))
	OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox"))) - 1
	TextEditBoxSetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox", towstring(OffsetY))
	Ges.TrackerIconSetAnchor(OffsetX, OffsetY)
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetUp",3)
end

function Ges.ExtrasTrackerOffsetRight(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasTrackerOffsetRight",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox"))) + 1
	OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox")))
	TextEditBoxSetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox", towstring(OffsetX))
	Ges.TrackerIconSetAnchor(OffsetX, OffsetY)
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetRight",3)
end

function Ges.ExtrasTrackerOffsetLeft(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasTrackerOffsetLeft",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox"))) - 1
	OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox")))
	TextEditBoxSetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox", towstring(OffsetX))
	Ges.TrackerIconSetAnchor(OffsetX, OffsetY)
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetLeft",3)
end

function Ges.ExtrasTrackerOffsetDown(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasTrackerOffsetDown",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox")))
	OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox"))) + 1
	TextEditBoxSetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox", towstring(OffsetY))
	Ges.TrackerIconSetAnchor(OffsetX, OffsetY)
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetDown",3)
end

function Ges.ExtrasTrackerOffsetXKeyEntry(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasTrackerOffsetXKeyEntry",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox")))
	OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox")))
	Ges.TrackerIconSetAnchor(OffsetX, OffsetY)
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetXKeyEntry",3)
end

function Ges.ExtrasTrackerOffsetYKeyEntry(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ExtrasTrackerOffsetYKeyEntry",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox")))
	OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox")))
	Ges.TrackerIconSetAnchor(OffsetX, OffsetY)
	Ges.DebugMsg(L"Leaving ExtrasTrackerOffsetYKeyEntry",3)
end

function Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Entering OptionsExtrasFeatureUpdate",3)
	local iconSelected     = ComboBoxGetSelectedMenuItem(Ges.Options.Extras.Window.."IconSelectionComboBox")
	local featureSelected  = ComboBoxGetSelectedMenuItem(Ges.Options.Extras.Window.."FeatureSelectionComboBox")
	local positionSelected = ComboBoxGetSelectedMenuItem(Ges.Options.Extras.Window.."FeaturePositionComboBox")
	local offsetX          = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."FeatureOffsetXEditBox")))
	local offsetY          = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."FeatureOffsetYEditBox")))

	if iconSelected == GesConstants.ICON_MONITOR then
		-- Save Monitor Icon Feature Settings
		Ges.Monitor.Extras[featureSelected].enabled = EnableExtraFeatureCheckToggle
		Ges.Monitor.Extras[featureSelected].anchor  = positionSelected
		Ges.Monitor.Extras[featureSelected].offsetX = offsetX
		Ges.Monitor.Extras[featureSelected].offsetY = offsetY
		Ges.ExtrasSetAnchor( Ges.Monitor.Extras[featureSelected].name
		                   , Ges.Monitor.Window
		                   , Ges.Monitor.Extras[featureSelected].anchor
		                   , Ges.Monitor.Extras[featureSelected].offsetX
		                   , Ges.Monitor.Extras[featureSelected].offsetY
		                   )
		Ges.MonitorIconSetTextures()
	elseif iconSelected == GesConstants.ICON_TRACKER then
		-- Save Tracker Icon Feature Settings
		Ges.Tracker.Extras[featureSelected].enabled = EnableExtraFeatureCheckToggle
		Ges.Tracker.Extras[featureSelected].anchor  = positionSelected
		Ges.Tracker.Extras[featureSelected].offsetX = offsetX
		Ges.Tracker.Extras[featureSelected].offsetY = offsetY
		Ges.ExtrasSetAnchor( Ges.Tracker.Extras[featureSelected].name
		                   , Ges.Tracker.Window
		                   , Ges.Tracker.Extras[featureSelected].anchor
		                   , Ges.Tracker.Extras[featureSelected].offsetX
		                   , Ges.Tracker.Extras[featureSelected].offsetY
		                   )
		Ges.TrackerIconSetTextures()
	else
		Ges.DebugMsg(L"OptionsExtrasFeatureUpdate: Invalid Selection",0)
	end
	Ges.DebugMsg(L"Leaving OptionsExtrasFeatureUpdate",3)
end

function Ges.OptionsExtrasReset()
	Ges.DebugMsg(L"Entering OptionsExtrasReset",3)

	for i,v in pairs(Ges.Monitor.Extras) do
		v.enabled = Ges.Settings.Monitor.Extras[i].enabled
		v.anchor  = Ges.Settings.Monitor.Extras[i].anchor
		v.offsetX = Ges.Settings.Monitor.Extras[i].offsetX
		v.offsetY = Ges.Settings.Monitor.Extras[i].offsetY
		Ges.ExtrasSetAnchor(v.name, Ges.Monitor.Window, v.anchor, v.offsetX, v.offsetY)
	end

	for i,v in pairs(Ges.Tracker.Extras) do
		v.enabled = Ges.Settings.Tracker.Extras[i].enabled
		v.anchor  = Ges.Settings.Tracker.Extras[i].anchor
		v.offsetX = Ges.Settings.Tracker.Extras[i].offsetX
		v.offsetY = Ges.Settings.Tracker.Extras[i].offsetY
		Ges.ExtrasSetAnchor(v.name, Ges.Tracker.Window, v.anchor, v.offsetX, v.offsetY)
	end
	-- Reset the textures based on the settings values.
	Ges.UpdateTargetDetails()
	-- Set the Tracker Icon back in case the user altered but didn't save
	Ges.TrackerIconSetAnchor(Ges.Settings.Tracker.OffsetX, Ges.Settings.Tracker.OffsetY)
	Ges.DebugMsg(L"Leaving OptionsExtrasReset",3)
end

function Ges.OptionsSoundsPlay(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering OptionsSoundsPlay",3)
	local button = SystemData.MouseOverWindow.name
	local choice = string.sub(button,-1)
	local soundIndex = ComboBoxGetSelectedMenuItem( Ges.Options.Sounds.Window.."TypeComboBox"..choice)
	if soundIndex > 1 then
		Ges.SoundsPlay(GesConstants.SoundTypes[soundIndex].id)
	end
	Ges.DebugMsg(L"Leaving OptionsSoundsPlay",3)
end

function Ges.OptionsClose(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering OptionsClose",3)
	-- Hide the Options window
	WindowSetShowing(Ges.Options.Window,false)
	-- Re-apply to Extras settings in case the user altered but didn't save
	Ges.OptionsExtrasReset()
	Ges.DebugMsg(L"Leaving OptionsClose",3)
end

function Ges.OptionsSave(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering OptionsSave",3)
	Ges.OptionsSaveGeneral()
	Ges.OptionsSaveColour()
	Ges.OptionsSaveExtras()
	Ges.OptionsSaveSounds()
	Ges.UpdateTargetDetails()
	Ges.DebugMsg(L"Leaving OptionsSave",3)
end

---------------------------------
--   Save Settings functions   --
---------------------------------

function Ges.OptionsSaveGeneral()
	Ges.DebugMsg(L"Entering OptionsSaveGeneral",3)
	Ges.Settings.Enabled = ButtonGetPressedFlag(Ges.Options.General.Window.."ModCheckBox")
	-- Check to see if the user has just changed the enable status of the mod and if so, alter the mod accordingly
	if (Ges.Enabled ~= Ges.Settings.Enabled) then Ges.Toggle() end
	-- Show/Hide the toggle button as the user has chosen
	Ges.Settings.ShowButton = ButtonGetPressedFlag(Ges.Options.General.Window.."ToggleCheckBox")
	WindowSetShowing(Ges.Button.Window,Ges.Settings.ShowButton)
	Ges.Settings.Sounds.Enabled = ButtonGetPressedFlag(Ges.Options.General.Window.."SoundEnableCheckBox")
	--
	Ges.Settings.ToggleColour = ComboBoxGetSelectedMenuItem(Ges.Options.General.Window.."ToggleColourComboBox")
	Ges.ToggleButtonSetTextures()
	-- Store channel and message for later use in Announce function.
	Ges.Settings.Channel = ComboBoxGetSelectedMenuItem(Ges.Options.General.Window.."ChannelsComboBox")
	Ges.Settings.Message = TextEditBoxGetText(Ges.Options.General.Window.."MessageEditBox")
	Ges.Settings.ShowButton = ButtonGetPressedFlag(Ges.Options.General.Window.."ToggleCheckBox")
	-- Enable/Disable Monitor Icon as the user has chosen
	if Ges.Settings.Monitor.Enabled ~= ButtonGetPressedFlag(Ges.Options.General.Window.."MonitorIconCheckBox") then
		Ges.DebugMsg(L"OptionsSave: Monitor Toggled",2)
		Ges.MonitorIconToggle(not Ges.Settings.Monitor.Enabled)
	end
	Ges.Settings.Monitor.Health.Enabled = ButtonGetPressedFlag(Ges.Options.General.Window.."MonitorHealthCheckBox")
	Ges.Settings.Monitor.Movable = not ButtonGetPressedFlag(Ges.Options.General.Window.."MonitorClickCheckBox")
	WindowSetMovable(Ges.Monitor.Window, Ges.Settings.Monitor.Movable)
	-- Enable/Disable Tracker Icon as the user has chosen
	if Ges.Settings.Tracker.Enabled ~= ButtonGetPressedFlag(Ges.Options.General.Window.."TrackerIconCheckBox") then
		Ges.DebugMsg(L"OptionsSave: Tracker Toggled",2)
		Ges.TrackerIconToggle(not Ges.Settings.Tracker.Enabled)
	end
	Ges.Settings.Tracker.Health.Enabled = ButtonGetPressedFlag(Ges.Options.General.Window.."TrackerHealthCheckBox")
	Ges.DebugMsg(L"Leaving OptionsSaveGeneral",3)
end

function Ges.OptionsSaveColour()
	Ges.DebugMsg(L"Entering OptionsSaveColour",3)
	local colourIconSelected = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."IconSelectionComboBox")

	if colourIconSelected == GesConstants.ICON_MONITOR then
		--------------------------------
		-- Save Monitor Icon contents --
		--------------------------------
		-- Store all of the values entered on the Monitor tab into the users settings.
		Ges.Settings.Monitor.DefaultIcon     = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."DefaultColourComboBox")
		Ges.Settings.Monitor.isOfflineIcon   = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."OfflineColourComboBox")
		Ges.Settings.Monitor.DeadIcon        = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."DeathColourComboBox")
		Ges.Settings.Monitor.Health.FullIcon = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."FullHealthColourComboBox")
		Ges.Settings.Monitor.Health.HighIcon = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."HighHealthColourComboBox")
		Ges.Settings.Monitor.Health.MedIcon  = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."MedHealthColourComboBox")
		Ges.Settings.Monitor.Health.MinIcon  = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."MinHealthColourComboBox")
		Ges.Settings.Monitor.Health.HighPerc = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Colour.Window.."HighHealthEditBox")))
		Ges.Settings.Monitor.Health.MedPerc  = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Colour.Window.."MedHealthEditBox")))
		Ges.Settings.Monitor.Health.MinPerc  = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Colour.Window.."MinHealthEditBox")))
		Ges.DebugMsg(L"OptionsSave: Monitor Icon settings saved",3)
	elseif colourIconSelected == GesConstants.ICON_TRACKER then
		--------------------------------
		-- Save Tracker Icon contents --
		--------------------------------
		-- Store all of the values entered on the Monitor tab into the users settings.
		Ges.Settings.Tracker.DefaultIcon     = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."DefaultColourComboBox")
		Ges.Settings.Tracker.isOfflineIcon   = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."OfflineColourComboBox")
		Ges.Settings.Tracker.DeadIcon        = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."DeathColourComboBox")
		Ges.Settings.Monitor.Health.FullIcon = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."FullHealthColourComboBox")
		Ges.Settings.Tracker.Health.HighIcon = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."HighHealthColourComboBox")
		Ges.Settings.Tracker.Health.MedIcon  = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."MedHealthColourComboBox")
		Ges.Settings.Tracker.Health.MinIcon  = ComboBoxGetSelectedMenuItem(Ges.Options.Colour.Window.."MinHealthColourComboBox")
		Ges.Settings.Tracker.Health.HighPerc = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Colour.Window.."HighHealthEditBox")))
		Ges.Settings.Tracker.Health.MedPerc  = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Colour.Window.."MedHealthEditBox")))
		Ges.Settings.Tracker.Health.MinPerc  = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Colour.Window.."MinHealthEditBox")))
	else
		Ges.DebugMsg(L"OptionsSaveColour: Invalid Selection",0)
	end
	Ges.DebugMsg(L"Leaving OptionsSaveColour",3)
end

function Ges.OptionsSaveExtras()
	Ges.DebugMsg(L"Entering OptionsSaveExtras",3)
	for i,v in pairs(Ges.Settings.Monitor.Extras) do
		v.enabled = Ges.Monitor.Extras[i].enabled
		v.anchor  = Ges.Monitor.Extras[i].anchor
		v.offsetX = Ges.Monitor.Extras[i].offsetX
		v.offsetY = Ges.Monitor.Extras[i].offsetY
	end

	for i,v in pairs(Ges.Settings.Tracker.Extras) do
		v.enabled = Ges.Tracker.Extras[i].enabled
		v.anchor  = Ges.Tracker.Extras[i].anchor
		v.offsetX = Ges.Tracker.Extras[i].offsetX
		v.offsetY = Ges.Tracker.Extras[i].offsetY
	end

	Ges.Settings.Tracker.OffsetX = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetXEditBox")))
	Ges.Settings.Tracker.OffsetY = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Extras.Window.."TrackerOffsetYEditBox")))
	Ges.DebugMsg(L"Leaving OptionsSaveExtras",3)
end

function Ges.OptionsSaveSounds()
	Ges.DebugMsg(L"Entering OptionsSaveSounds",3)
	Ges.Settings.Sounds.Applied = ComboBoxGetSelectedMenuItem(Ges.Options.Sounds.Window.."AppliedComboBox")
	Ges.Settings.Sounds.Removed = ComboBoxGetSelectedMenuItem(Ges.Options.Sounds.Window.."RemovedComboBox")
	for i,v in ipairs (Ges.Settings.Sounds.Preferences) do
		Ges.DebugMsg(L"OptionsSaveSounds: Saving Options "..i,3)
		Ges.Settings.Sounds.Preferences[i].threshold = assertNumber(tostring(TextEditBoxGetText(Ges.Options.Sounds.Window.."ThresholdEditBox"..i)))
		Ges.Settings.Sounds.Preferences[i].selection = ComboBoxGetSelectedMenuItem(Ges.Options.Sounds.Window.."TypeComboBox"..i)
		Ges.Settings.Sounds.Preferences[i].id        = GesConstants.SoundTypes[Ges.Settings.Sounds.Preferences[i].selection].id
	end
	Ges.SoundsInit()
	Ges.DebugMsg(L"Leaving OptionsSaveSounds",3)
end
-----------------------
--- Misc. Functions ---
-----------------------
-- Can't workout how to keep check box ticked via game functionality so here is a manual work around.
-- TODO: Locate the in-built in Mythic code to do this, it must exist somewhere.
function Ges.EnableModCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableModCheckToggle",3)
	EnableModCheckToggle = not EnableModCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."ModCheckBox",EnableModCheckToggle)
	Ges.DebugMsg(L"Leaving EnableModCheckToggle",3)
end

function Ges.EnableButtonCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableButtonCheckToggle",3)
	EnableButtonCheckToggle = not EnableButtonCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."ToggleCheckBox",EnableButtonCheckToggle)
	Ges.DebugMsg(L"Leaving EnableButtonCheckToggle",3)
end

function Ges.EnableDebugCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableDebugCheckToggle",3)
	EnableDebugCheckToggle = not EnableDebugCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."EnableDebugCheckBox",EnableDebugCheckToggle)
	Ges.DebugMsg(L"Leaving EnableDebugCheckToggle",3)
end

function Ges.EnableMonitorIconCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableMonitorIconCheckToggle",3)
	EnableMonitorIconCheckToggle = not EnableMonitorIconCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."MonitorIconCheckBox",EnableMonitorIconCheckToggle)
	Ges.DebugMsg(L"Leaving EnableMonitorIconCheckToggle",3)
end

function Ges.EnableMonitorHealthCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableMonitorHealthCheckToggle",3)
	EnableMonitorHealthCheckToggle = not EnableMonitorHealthCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."MonitorHealthCheckBox",EnableMonitorHealthCheckToggle)
	Ges.DebugMsg(L"Leaving EnableMonitorHealthCheckToggle",3)
end

function Ges.EnableMonitorClickCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableMonitorClickCheckToggle",3)
	EnableMonitorClickCheckToggle = not EnableMonitorClickCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."MonitorClickCheckBox",EnableMonitorClickCheckToggle)
	Ges.DebugMsg(L"Leaving EnableMonitorClickCheckToggle",3)
end

function Ges.EnableTrackerIconCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableTrackerIconCheckToggle",3)
	EnableTrackerIconCheckToggle = not EnableTrackerIconCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."TrackerIconCheckBox",EnableTrackerIconCheckToggle)
	Ges.DebugMsg(L"Leaving EnableTrackerIconCheckToggle",3)
end

function Ges.EnableTrackerHealthCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableTrackerHealthCheckToggle",3)
	EnableTrackerHealthCheckToggle = not EnableTrackerHealthCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."TrackerHealthCheckBox",EnableTrackerHealthCheckToggle)
	Ges.DebugMsg(L"Leaving EnableTrackerHealthCheckToggle",3)
end

function Ges.EnableSoundCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableSoundCheckToggle",3)
	EnableSoundCheckToggle = not EnableSoundCheckToggle
	ButtonSetPressedFlag(Ges.Options.General.Window.."SoundEnableCheckBox",EnableSoundCheckToggle)
	Ges.DebugMsg(L"Leaving EnableSoundCheckToggle",3)
end

function Ges.EnableExtraFeatureCheckToggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering EnableExtraFeatureCheckToggle",3)
	EnableExtraFeatureCheckToggle = not EnableExtraFeatureCheckToggle
	ButtonSetPressedFlag(Ges.Options.Extras.Window.."FeatureEnableCheckBox",EnableExtraFeatureCheckToggle)
	Ges.OptionsExtrasFeatureUpdate()
	Ges.DebugMsg(L"Leaving EnableExtraFeatureCheckToggle",3)
end
