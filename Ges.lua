local VERSION = 1.3	-- Set version of app for future changes/comparisons.

local function printMsg(str)
	EA_ChatWindow.Print(towstring(L"Ges: "..str))
end

local function boolToString(bool)
	if bool then return "true" end
	return "false"
end

--Check if the Ges object already exists and if not create it
if (not Ges) then
	Ges = {}
	Ges.Debug = {}
	Ges.Debug.Enabled = false
	Ges.Debug.Level   = 2

	Ges.Ability = {}
	Ges.AbilityExists = false
	Ges.AbilityList = {
	      [1363] = "Guard^n"
	    , [1674] = "Save Da Runts^n"
	    , [8013] = "Guard^n"
	    , [8325] = "Guard^n"
	    , [9008] = "Guard^n"
	    , [9325] = "Guard^n"
	                  }
	Ges.Buff = {}
	Ges.Buff.Active = false
	Ges.Buff.Id     = nil
	Ges.Buff.Name   = ""

	Ges.Button = {}
	Ges.Button.Window  = "GesToggle"
	Ges.Button.Btn     = Ges.Button.Window.."Button"
	Ges.Button.Image   = Ges.Button.Window.."Image"
	Ges.Button.Hilight = Ges.Button.Window.."Hilight"

	Ges.CheckCasting = false
	Ges.Enabled      = true
	Ges.Registered   = false
	Ges.ZoneId       = 0

	Ges.Sounds = {}
	Ges.Sounds.Preferences = {
		   [GesConstants.SOUNDS_PREF_1] = { played = false , id = nil , threshold = 0 }
		 , [GesConstants.SOUNDS_PREF_2] = { played = false , id = nil , threshold = 0 }
		 , [GesConstants.SOUNDS_PREF_3] = { played = false , id = nil , threshold = 0 }
		 , [GesConstants.SOUNDS_PREF_4] = { played = false , id = nil , threshold = 0 }
	             }
	Ges.Sounds.AppliedId = nil
	Ges.Sounds.RemovedId = nil

	Ges.Monitor = {}
	Ges.Monitor.Window = "GesMonitor"
	Ges.Monitor.Image = {}
	Ges.Monitor.Image.Name    = Ges.Monitor.Window.."Image"
	Ges.Monitor.Image.Current = 0
	Ges.Monitor.Hilight = {}
	Ges.Monitor.Hilight.Name = Ges.Monitor.Window.."Hilight"
	Ges.Monitor.Hilight.Current = 0
	Ges.Monitor.Status = {}
	Ges.Monitor.Status.Name = Ges.Monitor.Window.."Status"
	Ges.Monitor.Status.Current = GesConstants.FOREGROUND_BLANK
	Ges.Monitor.Extras = {
		  [GesConstants.EXTRAS_TARGET_HEALTH] = { name = Ges.Monitor.Window.."TargetHealth" , enabled = false , anchor = 0 , offsetX = 0 , offsetY = 0}
		, [GesConstants.EXTRAS_TARGET_NAME]   = { name = Ges.Monitor.Window.."TargetName"   , enabled = false , anchor = 0 , offsetX = 0 , offsetY = 0}
		, [GesConstants.EXTRAS_TARGET_CAREER] = { name = Ges.Monitor.Window.."CareerIcon"   , enabled = false , anchor = 0 , offsetX = 0 , offsetY = 0}
	                     }

	Ges.Tracker = {}
	Ges.Tracker.Active = false
	Ges.Tracker.Anchor = "GesTrackerAnchor"
	Ges.Tracker.Window = "GesTracker"
	Ges.Tracker.Layout = Ges.Tracker.Window.."LayoutEditor"
	Ges.Tracker.EntityId = 0
	Ges.Tracker.Image = {}
	Ges.Tracker.Image.Name = Ges.Tracker.Window.."Image"
	Ges.Tracker.Image.Current = 1
	Ges.Tracker.Hilight = {}
	Ges.Tracker.Hilight.Name = Ges.Tracker.Window.."Hilight"
	Ges.Tracker.Hilight.Current = GesConstants.FOREGROUND_HILIGHT
	Ges.Tracker.Status = {}
	Ges.Tracker.Status.Name = Ges.Tracker.Window.."Status"
	Ges.Tracker.Status.Current = GesConstants.FOREGROUND_BLANK
	Ges.Tracker.Extras = {
		  [GesConstants.EXTRAS_TARGET_HEALTH] = { name = Ges.Tracker.Window.."TargetHealth" , enabled = false , anchor = 0 , offsetX = 0 , offsetY = 0}
		, [GesConstants.EXTRAS_TARGET_NAME]   = { name = Ges.Tracker.Window.."TargetName"   , enabled = false , anchor = 0 , offsetX = 0 , offsetY = 0}
		, [GesConstants.EXTRAS_TARGET_CAREER] = { name = Ges.Tracker.Window.."CareerIcon"   , enabled = false , anchor = 0 , offsetX = 0 , offsetY = 0}
	                     }

	Ges.Target = {}
	Ges.Target.CareerLine     = nil
	Ges.Target.CareerTexture  = nil
	Ges.Target.CareerOffsetX  = nil
	Ges.Target.CareerOffsetY  = nil
	Ges.Target.EntityId       = 0
	Ges.Target.EntityIdNew    = 0
	Ges.Target.EntityIdOld    = 0
	Ges.Target.HealthPerc     = 0
	Ges.Target.healthDecrease = false
	Ges.Target.isActive       = false
	Ges.Target.isDistant      = false
	Ges.Target.isDead         = false
	Ges.Target.isOnline       = false
	Ges.Target.isInSameRegion = false
	Ges.Target.Name           = ""
	Ges.Target.NameNew        = ""
end

----------------------------
--- Core Addon Functions ---
----------------------------

-- This function is called whenever the addon is loaded.
function Ges.Init()
	Ges.DebugMsg(L"Entering Init",3)
	-- Check to see if any options are saved on the users profile.  If not set default values.
	if (not Ges.Settings) then
		Ges.Settings = {}
		Ges.Settings.Channel = 2
		Ges.Settings.Enabled = true
		Ges.Settings.ToggleColour = 7
		Ges.Settings.Message = L""
		Ges.Settings.ShowButton = true

		Ges.Settings.Sounds = {}
		Ges.Settings.Sounds.Enabled = false
		Ges.Settings.Sounds.Preferences = {
		   [GesConstants.SOUNDS_PREF_1] = { selection = GesConstants.SELECTION_DISABLED , id = nil , threshold = 0 }
		 , [GesConstants.SOUNDS_PREF_2] = { selection = GesConstants.SELECTION_DISABLED , id = nil , threshold = 0 }
		 , [GesConstants.SOUNDS_PREF_3] = { selection = GesConstants.SELECTION_DISABLED , id = nil , threshold = 0 }
		 , [GesConstants.SOUNDS_PREF_4] = { selection = GesConstants.SELECTION_DISABLED , id = nil , threshold = 0 }
		                                  }
		Ges.Settings.Monitor = {}
		Ges.Settings.Monitor.Enabled = true
		Ges.Settings.Monitor.DefaultIcon   = 6
		Ges.Settings.Monitor.DeadIcon      = 9
		Ges.Settings.Monitor.isOfflineIcon = 9
		Ges.Settings.Monitor.Health = {}
		Ges.Settings.Monitor.Health.Enabled  = true
		Ges.Settings.Monitor.Health.FullIcon = 4
		Ges.Settings.Monitor.Health.HighIcon = 3
		Ges.Settings.Monitor.Health.MedIcon  = 2
		Ges.Settings.Monitor.Health.MinIcon  = 5
		Ges.Settings.Monitor.Health.HighPerc = 90
		Ges.Settings.Monitor.Health.MedPerc  = 75
		Ges.Settings.Monitor.Health.MinPerc  = 50
		Ges.Settings.Monitor.Extras = {
		   [GesConstants.EXTRAS_TARGET_HEALTH] = { enabled = true  , anchor = 3 , offsetX = 0 , offsetY = 0}
		 , [GesConstants.EXTRAS_TARGET_NAME]   = { enabled = true  , anchor = 1 , offsetX = 0 , offsetY = 0}
		 , [GesConstants.EXTRAS_TARGET_CAREER] = { enabled = false , anchor = 3 , offsetX = 0 , offsetY = 0}
		                              }

		Ges.Settings.Tracker = {}
		Ges.Settings.Tracker.Enabled = true
		Ges.Settings.Tracker.DefaultIcon   = 6
		Ges.Settings.Tracker.DeadIcon      = 9
		Ges.Settings.Tracker.isOfflineIcon = 9
		Ges.Settings.Tracker.Health = {}
		Ges.Settings.Tracker.Health.Enabled  = true
		Ges.Settings.Tracker.Health.FullIcon = 4
		Ges.Settings.Tracker.Health.HighIcon = 3
		Ges.Settings.Tracker.Health.MedIcon  = 2
		Ges.Settings.Tracker.Health.MinIcon  = 5
		Ges.Settings.Tracker.Health.HighPerc = 90
		Ges.Settings.Tracker.Health.MedPerc  = 75
		Ges.Settings.Tracker.Health.MinPerc  = 50
		Ges.Settings.Tracker.Extras = {
		   [GesConstants.EXTRAS_TARGET_HEALTH] = { enabled = false , anchor = 5 , offsetX = 0 , offsetY = 0 }
		 , [GesConstants.EXTRAS_TARGET_NAME]   = { enabled = false , anchor = 1 , offsetX = 0 , offsetY = 0 }
		 , [GesConstants.EXTRAS_TARGET_CAREER] = { enabled = false , anchor = 3 , offsetX = 0 , offsetY = 0 }
		                              }
		Ges.Settings.Tracker.OffsetX = 0
		Ges.Settings.Tracker.OffsetY = 0
		Ges.Settings.Version = 1.11
	end
	if Ges.Settings.Version < 1.12 then
		Ges.Settings.Monitor.Movable = true
		Ges.Settings.Version = 1.12
	end
	if Ges.Settings.Version < 1.21 then
		Ges.Settings.Sounds.Applied = GesConstants.SELECTION_DISABLED
		Ges.Settings.Sounds.Removed = GesConstants.SELECTION_DISABLED
		Ges.Settings.Version = 1.21
	end
	Ges.Settings.Version = VERSION
	-- Save a local version of enabled flag so that Ges can turn itself off if a none tank
    -- character is loaded, without the disable affecting the users profile settings.
	Ges.Enabled = Ges.Settings.Enabled
	-- Register the slash commands
	LibSlash.RegisterSlashCmd("ges", Ges.Slash)
	LibSlash.RegisterSlashCmd("gesd", function(args) Ges.SlashDebug(args) end)
	-- If Ges isn't turned off in the settings then add event hooks
	if (Ges.Enabled) then
		Ges.RegisterEvents()
		printMsg(L"Is enabled. Type /ges for options")
	else
		printMsg(L"Is disabled. Type /ges for options")
	end
	-- Iniitalise the visual windows
	Ges.OptionsWindowInit()
	Ges.ToggleButtonInit()
	Ges.MonitorIconInit()
	Ges.TrackerIconInit()
	-- Initialise the Sound settings
	Ges.SoundsInit()
	-- Track the loading of the end of the mods inorder to complete initialisation
	RegisterEventHandler(SystemData.Events.LOADING_END, "Ges.InitComplete")
	Ges.DebugMsg(L"Leaving Init",3)
end

function Ges.InitComplete()
	Ges.DebugMsg(L"Entering InitComplete",3)
	Ges.FindGuardAbility()
	Ges.CommsInitCompelte()
	-- Init complete, prevent this function from being called again.
	UnregisterEventHandler(SystemData.Events.LOADING_END, "Ges.InitComplete")
	Ges.DebugMsg(L"Leaving InitComplete",3)
end

function Ges.DebugMsg(str,lvl)
   -- If debug is enabled and the level of the message is high enough then print the string to the chat window
	if (Ges.Debug.Enabled and lvl <= Ges.Debug.Level) then
		d(towstring(str))
	end
end

function Ges.SlashDebug(args)
	Ges.DebugMsg(L"Entering SlashDebug",3)
	local arg, argv = args:match("([a-z0-9]+)[ ]?(.*)")
	Ges.DebugMsg("SlashDebug: arg ["..arg.."] argv ["..argv.."]",2)
	if arg == nil then
		Ges.Debug.Enabled = not Ges.Debug.Enabled
		return
	end
	-- Set to true to display debug messages in the debug window (Note: type /debug in game to see this window and press the button "Logs(off)")
	-- Set the debug level (4 = Update Ticks, 3 = Position, 2 = Setting values, 1 = Return values, 0 = Illegal flow catches)
	if arg == "on" then
		Ges.Debug.Enabled = true
	elseif arg == "off" then
		Ges.Debug.Enabled = false
	elseif arg == "1" then
		Ges.Debug.Enabled = true
		Ges.Debug.Level = 1
	elseif arg == "2" then
		Ges.Debug.Enabled = true
		Ges.Debug.Level = 2
	elseif arg == "3" then
		Ges.Debug.Enabled = true
		Ges.Debug.Level = 3
	elseif arg == "4" then
		Ges.Debug.Enabled = true
		Ges.Debug.Level = 4
	else
		Ges.DebugMsg("SlashDebug: Invalid Argument ["..arg.."]",0)
	end
	Ges.DebugMsg(L"Leaving SlashDebug",3)
end

function Ges.RegisterEvents()
	Ges.DebugMsg(L"Entering RegisterEvents",3)
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST     , "Ges.StartCast")
	RegisterEventHandler(SystemData.Events.PLAYER_END_CAST       , "Ges.EndCast")
	RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "Ges.GuardBuffTracker")
	RegisterEventHandler(SystemData.Events.GROUP_UPDATED         , "Ges.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.GROUP_STATUS_UPDATED  , "Ges.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_UPDATED   , "Ges.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_MEMBER_UPDATED, "Ges.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.GROUP_LEAVE           , "Ges.DeactivateOverride")
	RegisterEventHandler(SystemData.Events.SCENARIO_GROUP_LEAVE  , "Ges.DeactivateOverride")
	RegisterEventHandler(SystemData.Events.LOADING_BEGIN         , "Ges.TrackZoneBegin")
	RegisterEventHandler(SystemData.Events.LOADING_END           , "Ges.TrackZoneEnd")
	RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED     , "Ges.CommsReceived")
	LayoutEditor.RegisterEditCallback(Ges.TrackerIconLayoutUpdate)
	Ges.Registered = true
	Ges.DebugMsg(L"Leaving RegisterEvents",3)
end

function Ges.UnRegisterEvents()
	Ges.DebugMsg(L"Entering UnRegisterEvents",3)
	if Ges.Registered then
		UnregisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST     , "Ges.StartCast")
		UnregisterEventHandler(SystemData.Events.PLAYER_END_CAST       , "Ges.EndCast")
		UnregisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "Ges.GuardBuffTracker")
		UnregisterEventHandler(SystemData.Events.GROUP_UPDATED         , "Ges.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.GROUP_STATUS_UPDATED  , "Ges.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.BATTLEGROUP_UPDATED   , "Ges.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.BATTLEGROUP_MEMBER_UPDATED, "Ges.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.GROUP_LEAVE           , "Ges.DeactivateOverride")
		UnregisterEventHandler(SystemData.Events.SCENARIO_GROUP_LEAVE  , "Ges.DeactivateOverride")
		UnregisterEventHandler(SystemData.Events.LOADING_BEGIN         , "Ges.TrackZoneBegin")
		UnregisterEventHandler(SystemData.Events.LOADING_END           , "Ges.TrackZoneEnd")
		UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED     , "Ges.CommsReceived")
		Ges.Registered = false
	end
	Ges.DebugMsg(L"Leaving UnRegisterEvents",3)
end

function Ges.Shutdown()
	Ges.DebugMsg(L"Entering Shutdown",3)
	-- Mod is shutdown, deactivate and unsubscribe from events.
	Ges.Enabled = false
	Ges.DeactivateOverride()
	Ges.UnRegisterEvents()
	Ges.DebugMsg(L"Leaving Shutdown",3)
end

function Ges.Slash()
	Ges.DebugMsg(L"Entering Slash",3)
	-- When user types /ges show the Options window.
	Ges.OptionsShow()
	Ges.DebugMsg(L"Leaving Slash",3)
end

function Ges.OnUpdate(timePassed)
	if not Ges.Enabled then return end
	Ges.CommsUpdate(timePassed)
end

----------------------------------
--- Guard Activation Functions ---
----------------------------------
function Ges.FindGuardAbility()
	Ges.DebugMsg(L"Entering FindGuardAbility",3)
	-- Retrieve the players abilities
	local abilityTable = GetAbilityTable(GameData.AbilityType.STANDARD)
	-- Traverse through the abilities looking for one of the 6 Guard abilities
	for i, v in pairs(Ges.AbilityList) do
		if abilityTable[i] ~= nil then
			Ges.DebugMsg(L"FindGuardAbility: Ability found ["..abilityTable[i]["name"]..L"]",2)
			-- A Guard ability has been located, store its details and flag it's existance.
			Ges.Ability[i] = abilityTable[i]["name"]
			printMsg(L"Guard ability found: "..abilityTable[i]["name"]);
			Ges.AbilityExists = true
			Ges.DebugMsg(L"Leaving FindGuardAbility",3)
			return
		end
	end
	Ges.DebugMsg(L"FindGuardAbility: Ability not found",2)
	-- Guard ability has not been located.
	Ges.AbilityExists = false
	Ges.DebugMsg(L"Leaving FindGuardAbility",3)
end

function Ges.StartCast(abilityId, isChanneledSpell, desiredCastTime, averageLatency)
	Ges.DebugMsg(L"Entering StartCast",3)
	if not Ges.AbilityExists then
		-- Check to see if the character has a Ges ability
		Ges.FindGuardAbility()
		if not Ges.AbilityExists then
			-- No Guard ability found, so disable mod, unhook and return.
			Ges.Shutdown()
			printMsg(L"Guard ability not located. Ges has turned itself off.");
			return
		end
	end
	-- Check that the ability being cast is the Ges ability and if friendly target is valid
	if Ges.Ability[abilityId] ~= nil then
		local targetName = string.match(tostring(TargetInfo:UnitName("selffriendlytarget")),"%a+")
		if targetName ~= nil and targetName ~= "nil" then
			-- A valid Guard ability is being cast, register to check if the casting is successful
			Ges.DebugMsg(L"StartCase: Valid guard activity, now monitoring the cast",2)
			-- In case Ges is already active on another target, deactivate before activating
			Ges.Deactivate(false)
			Ges.Target.NameNew     = targetName
			Ges.Target.EntityIdNew = TargetInfo:UnitEntityId("selffriendlytarget")
			Ges.Buff.Name          = Ges.Ability[abilityId]
			Ges.CheckCasting       = true
		end
	end
	Ges.DebugMsg(L"Leaving StartCast",3)
end

function Ges.EndCast(CastFailed)
	Ges.DebugMsg(L"Entering EndCast",3)
	if not Ges.CheckCasting then return end
	Ges.CheckCasting = false
	-- Check to see if Ges should be Announcing or not
	if Ges.Enabled then
		Ges.Target.EntityId    = Ges.Target.EntityIdNew
		Ges.Target.Name        = Ges.Target.NameNew
		Ges.Target.EntityIdNew = 0
		Ges.Target.NameNew     = nil
		--Ges.Announce(GesConstants.STATUS_APPLIED,CastFailed)
		if CastFailed then
			local commsDetails = Ges.CommsCreateDetails(GesConstants.STATUS_APPLIED, GesConstants.CAST_FAILED)
			Ges.CommsAddToQueue(GesConstants.COMMS_TYPE_ANNOUNCE, commsDetails)
		else
			local commsDetails = Ges.CommsCreateDetails(GesConstants.STATUS_APPLIED, GesConstants.CAST_SUCCESS)
			Ges.CommsAddToQueue(GesConstants.COMMS_TYPE_SEND    , commsDetails)
			Ges.CommsAddToQueue(GesConstants.COMMS_TYPE_ANNOUNCE, commsDetails)
			Ges.Activate()
		end
	end
	Ges.DebugMsg(L"Leaving EndCast",3)
end

function Ges.Activate()
	Ges.DebugMsg(L"Entering Activate",3)
	-- Record the details of the new target and activate the icons
	Ges.Target.isActive    = true
	if Ges.Settings.Monitor.Enabled then
		Ges.MonitorIconActivate()
	end
	if Ges.Settings.Tracker.Enabled then
		Ges.TrackerIconActivate(Ges.Target.EntityId)
	end
	-- Prompt the first update to get everything runnning.
	Ges.UpdateTargetDetails(nil)
	Ges.SoundsApplied()
	Ges.DebugMsg(L"Leaving Activate",3)
end

function Ges.Deactivate(announce)
	Ges.DebugMsg(L"Entering Deactivate",3)
	-- Check if mod has an active target if so, turn everything off
	if Ges.Target.isActive then
		local commsDetails = Ges.CommsCreateDetails(GesConstants.STATUS_REMOVED, GesConstants.CAST_SUCCESS)
		Ges.CommsAddToQueue(GesConstants.COMMS_TYPE_SEND, commsDetails)
		Ges.Target.isActive = false
		Ges.MonitorIconDeactivate()
		Ges.TrackerIconDeactivate()
		if announce then
			--Ges.Announce(GesConstants.STATUS_REMOVED,true)
			Ges.CommsAddToQueue(GesConstants.COMMS_TYPE_ANNOUNCE, commsDetails)
			Ges.SoundsRemoved()
		end
		-- Set everything back to it's default position
		Ges.Target.CareerLine     = nil
		Ges.Target.CareerTexture  = nil
		Ges.Target.CareerOffsetX  = nil
		Ges.Target.CareerOffsetY  = nil
		Ges.Target.EntityId       = 0
		Ges.Target.EntityIdOld    = 0
		Ges.Target.HealthPerc     = 0
		Ges.Target.healthDecrease = false
		Ges.Target.isDead         = false
		Ges.Target.isDistant      = false
		Ges.Target.isInSameRegion = false
		Ges.Target.isOnline       = false
		Ges.Target.Name           = ""
	end
	if Ges.Buff.Active then
		-- Reset the Buff details so the BuffTracker ceases.
		Ges.Buff.Active = false
		Ges.Buff.Id = nil
	end
	-- Reset the sounds for the new target.
	Ges.SoundsReset()
	Ges.DebugMsg(L"Leaving Deactivate",3)
end

function Ges.DeactivateOverride()
	Ges.DebugMsg(L"Entering DeactivateOverride",3)
	Ges.Deactivate(false)
	Ges.DebugMsg(L"Leaving DeactivateOverride",3)
end

function Ges.TrackZoneBegin()
	Ges.DebugMsg(L"Entering TrackZoneBegin",3)
	Ges.ZoneId = GameData.Player.zone
	Ges.DebugMsg(L"Leaving TrackZoneBegin",3)
end

function Ges.TrackZoneEnd()
	Ges.DebugMsg(L"Entering TrackZoneEnd",3)
	-- This function captures events like zoning that cancel the guard buff but is missed by the bufftracker due to Warhammer API
	if GameData.Player.zone ~= Ges.ZoneId then
		Ges.Deactivate(false)
	end
	Ges.DebugMsg(L"Leaving TrackZoneEnd",3)
end

--TODO: Remove if new version works
function Ges.Announce(State,castFailed)
	Ges.DebugMsg(L"Entering Announce",3)
	Ges.DebugMsg("Announce: State=["..State.."] castFailed=["..boolToString(castFailed).."]",2)
	local message = ""
	local channel = towstring(GesConstants.Channels[Ges.Settings.Channel].channel)
	Ges.DebugMsg(L"Announce: Channel["..channel..L"]",2)
	if (channel == L"") then
		Ges.DebugMsg("Announce: User has disabled announcements, exiting",3)
		return
	end
	-- Set the text to be sent
	if State == GesConstants.STATUS_APPLIED then
		if castFailed then
			Ges.DebugMsg(L"Announce: Casting Unsuccessful",2)
			message = L"Guarding "..towstring(Ges.Target.NameNew)..L" was unsuccessful."
		else
			Ges.DebugMsg(L"Announce: Casting Successful",2)
			message = L"Guarding "..towstring(Ges.Target.NameNew)..L" "..towstring(Ges.Settings.Message)
		end
	elseif State == GesConstants.STATUS_REMOVED then
		Ges.DebugMsg(L"Announce: Guard has been removed",2)
		message = L"Guard has been removed from "..towstring(Ges.Target.Name)
	end
	-- Check to see which channel has been selected and set chat to the desired channel
	if (channel == L"sm") then
		Ges.DebugMsg(L"Announce: Smart channel detected",2)
		-- Ascertain users status and set to appropriate channel.
		if (GameData.Player.isInScenario) then
			Ges.DebugMsg(L"Announce: Channel set to /sp",2)
			SystemData.UserInput.ChatChannel = L"/sp"
		else
			Ges.DebugMsg(L"Announce: Channel set to /p",2)
			SystemData.UserInput.ChatChannel = L"/p"
		end
	elseif (channel ~= L"d") then
		SystemData.UserInput.ChatChannel = L"/"..channel
	end
	-- Check to see if user is going to announce on the Advice channel
	-- If so redirect to the /party channel instead
	if (SystemData.UserInput.ChatChannel == L"/ad") then
		SystemData.UserInput.ChatChannel = L"/p"
	end
	-- Send the text
	SystemData.UserInput.ChatText = message
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
	Ges.DebugMsg(L"Leaving Announce",3)
end

------------------------------
--- Guard Update Functions ---
------------------------------

-- Function is called when a group member is updated
function Ges.UpdateTargetDetails(groupMemberIndex)
	Ges.DebugMsg(L"Entering UpdateTargetDetails",3)
	if not Ges.Target.isActive then return end
	local targetDetails = nil

	--[[
		As at 1.3.4 Mythic have a bug with the Party data where if someone goes out of range and comes back and
		the party data hasn't become dirty, then that person has lost their Entity ID and in the case of warbands
		they can show up as isDistant == true when they are standing right next to you.  This code goes someway to
		working around this issue but it is not perfect by any means.
	--]]
	-- Check if guard target is the current friendly target and if so use it's details instead of party details
	if Ges.Target.Name == string.match(tostring(TargetInfo:UnitName("selffriendlytarget")),"%a+") then
		Ges.DebugMsg("UTD: Using TragetInfo details",2)
		targetDetails = {}
		targetDetails.worldObjNum = TargetInfo:UnitEntityId("selffriendlytarget")
		targetDetails.isDistant = false
		targetDetails.isInSameRegion = true
		targetDetails.online = true
		targetDetails.healthPercent = TargetInfo:UnitHealth("selffriendlytarget")
		targetDetails.careerLine = TargetInfo:UnitCareer("selffriendlytarget")
		targetDetails.name = towstring(Ges.Target.Name)
	else
		-- Retrieve the targets data from the party data
		Ges.DebugMsg(L"UTD: Getting all party details",2)
		for i,v in pairs (PartyUtils.GetPartyData()) do
			Ges.DebugMsg("UTD: Retrieved details for ["..tostring(v.name).."] Target name ["..Ges.Target.Name.."]",2)
			if tostring(v.name) == Ges.Target.Name then
				Ges.DebugMsg(L"UTD: Targets details have been found",2)
				targetDetails = v
				break
			end
		end
	end
	-- Check if the targetDetails are valid
	if targetDetails == nil or targetDetails.name == nil or targetDetails.name == L"" then
		Ges.DebugMsg(L"UTD - Party Member details are invalid",0)
		return
	end

	Ges.DebugMsg("UTD - -------- Target Values --------",2)
	Ges.DebugMsg(L"UTD - Name ["..targetDetails.name..L"]",2)
	Ges.DebugMsg("UTD - isDistant ["..boolToString(targetDetails.isDistant).."]",2)
	Ges.DebugMsg("UTD - isInSameRegion ["..boolToString(targetDetails.isInSameRegion).."]",2)
	Ges.DebugMsg("UTD - isonline ["..boolToString(targetDetails.online).."]",2)
	Ges.DebugMsg(L"UTD - HealthPerc ["..targetDetails.healthPercent..L"]",2)
	Ges.DebugMsg(L"UTD - World Object ID ["..towstring(targetDetails.worldObjNum)..L"]",2)
	Ges.DebugMsg("UTD - -------- Target Values --------",2)

	-- Store the targets latest details.
	if Ges.Target.EntityId ~= 0 or targetDetails.worldObjNum ~= 0 then
		Ges.Target.EntityIdOld = Ges.Target.EntityId
	end
	Ges.Target.EntityId       = targetDetails.worldObjNum
	Ges.Target.isDistant      = targetDetails.isDistant
	Ges.Target.isInSameRegion = targetDetails.isInSameRegion
	Ges.Target.isOnline       = targetDetails.online
	Ges.Target.healthDecrease = Ges.Target.HealthPerc > tonumber(tostring(targetDetails.healthPercent))
	Ges.Target.HealthPerc     = tonumber(tostring(targetDetails.healthPercent))
	Ges.Target.isDead         = Ges.Target.HealthPerc == 0
	if Ges.Target.CareerLine == nil then
		Ges.Target.CareerLine = targetDetails.careerLine
		Ges.Target.CareerTexture, Ges.Target.CareerOffsetX, Ges.Target.CareerOffsetY = GetIconData(Icons.GetCareerIconIDFromCareerLine(Ges.Target.CareerLine ))
	end
	-- Update the icons and sounds.
	Ges.MonitorIconUpdate()
	Ges.TrackerIconUpdate()
	Ges.SoundsUpdate()
	Ges.DebugMsg(L"Leaving UpdateTargetDetails",3)
end

function Ges.GuardBuffTracker(updatedBuffsTable, isFullList)
	-- Check to see if there is a need to be tracking the buffs, if not exit.
	if not Ges.Buff.Active and not Ges.CheckCasting then return end
	Ges.DebugMsg(L"Entering GuardBuffTracker",3)
	if( not updatedBuffsTable ) then
		Ges.DebugMsg(L"Invalid updatedBuffsTable",2)
		Ges.DebugMsg(L"Leaving GuardBuffTracker",3)
		return
	end
	-- Cycle through the list of buffs provided.
	local buffExists = false
	for buffId, buffData in pairs( updatedBuffsTable ) do
		if Ges.Buff.Id == nil then
			Ges.DebugMsg(L"BuffTracker: Ges.Buff.Id is not set",2)
			if buffData.name ~= nil then
				Ges.DebugMsg(L"BuffTracker: Buff is valid",2)
				-- Check to see if the Buffs name matches the Guard ability name
				Ges.DebugMsg(L"BuffTracker: Buff ["..buffData.name..L"] Ability ["..Ges.Buff.Name..L"]",2)
				if buffData.name == Ges.Buff.Name then
					Ges.Buff.Active = true
					Ges.Buff.Id = buffId
					Ges.DebugMsg(L"BuffTracker: Name:["..Ges.Buff.Name..L"] has been found",2)
					Ges.DebugMsg(L"Leaving GuardBuffTracker",3)
					return
				end
			end
			Ges.DebugMsg(L"BuffTracker: Not the guard buff though",2)
		elseif buffId == Ges.Buff.Id then
			if buffData.name == nil then
				Ges.Deactivate(true)
				Ges.DebugMsg(L"BuffTracker: Buff ["..Ges.Buff.Name..L"] has been turned off",2)
				Ges.DebugMsg("Leaving GuardBuffTracker",3)
				return
			else
				buffExists = true
				Ges.DebugMsg(L"BuffTracker: Buff ["..buffData.name..L"] is still active",2)
				break
			end
		end
	end
	if not buffExists and Ges.Buff.Id ~= nil and isFullList then
		Ges.Deactivate(true)
		Ges.DebugMsg(L"BuffTracker: Buff ["..Ges.Buff.Name..L"] no longer exists and Ges has turned itself off",2)
	end
	Ges.DebugMsg(L"Leaving GuardBuffTracker",3)
end

---------------------------------------------
---------- UI Specific Functions ------------
---------------------------------------------

-------------------------------
--- Toggle Button Functions ---
-------------------------------
function Ges.ToggleButtonInit()
	Ges.DebugMsg(L"Entering ToggleButtonInit",3)
	-- Create and register the toggle button window and then show/hide it based on the settings.
	CreateWindow(Ges.Button.Window, true)
	-- This is not a typo, for some reason the ButtonSetTextures function only works if
	-- the window is first hidden and then shown, no idea why and it was a pain to debug.
	WindowSetShowing(Ges.Button.Window,false)
	WindowSetShowing(Ges.Button.Window,Ges.Settings.ShowButton)
	-- Register window with the layout editor so that it can be moved and resized there.
	LayoutEditor.RegisterWindow ( Ges.Button.Window			-- windowName
	                            , L"Ges Button" 				-- windowDisplayName
	                            , L"The toggle button for Ges"	-- windowDesc
	                            , false 						-- allowSizeWidth
	                            , false 						-- allowSizeHeight
	                            , true  						-- allowHiding
	                            , nil 							-- allowableAnchorList
	                            )
	Ges.ToggleButtonSetTextures()
	Ges.DebugMsg(L"Leaving ToggleButtonInit",3)
end

function Ges.ToggleButtonSetTextures()
	Ges.DebugMsg(L"Entering ToggleButtonSetTextures",3)
	-- Set the background texture and slice for the Toggle Button
	DynamicImageSetTexture( Ges.Button.Image
	                      , GesConstants.IconsBackgrounds[Ges.Settings.ToggleColour].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( Ges.Button.Image
	                           , GesConstants.IconsBackgrounds[Ges.Settings.ToggleColour].slice
	                           )
	-- Set the foreground texture and slice for the Toggle Button for both normal and highlighted conditions.
	ButtonSetTextureSlice( Ges.Button.Btn
	                     , Button.ButtonState.NORMAL
	                     , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_TEXTBG].texture
	                     , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_TEXTBG].slice
	                     )
	-- Set the foreground texture and slice for the Monitor Icon
	DynamicImageSetTexture( Ges.Button.Hilight
	                      , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_HILIGHT].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( Ges.Button.Hilight
	                           , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_HILIGHT].slice
	                           )
	Ges.DebugMsg(L"Leaving ToggleButtonSetTextures",3)
end

function Ges.Toggle(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering Toggle",3)
	-- Check the status of Ges and alter it to the opposite
	if Ges.Enabled then
		-- Disable Ges
		Ges.Shutdown()
		Ges.Settings.Enabled = false
	else
		Ges.Settings.Enabled = true
		Ges.FindGuardAbility()
		-- Enable Ges only if Guard ability exists
		if Ges.AbilityExists then
			Ges.RegisterEvents()
			Ges.Enabled = true
			if Ges.Buff.Active then
				Ges.Activate()
			end
		else
			printMsg(L"Guard ability not located. Ges has turned itself back off.");
			Ges.Enabled = false
		end
	end
	Ges.ToggleButtonMouseOver()
	Ges.DebugMsg(L"Leaving Toggle",3)
end

function Ges.ToggleButtonMouseOver(flags, mouseX, mouseY)
	Ges.DebugMsg(L"Entering ToggleButtonMouseOver",3)
	local TextStatus  = L""
	local TextToggle  = L""
	local TextAbility = L""
	local TextChannel = towstring(GesConstants.Channels[Ges.Settings.Channel].desc)
	-- Generate the tooltip message
	if Ges.Enabled then
		 TextStatus = L"Enabled"
		 TextToggle = L"Disable"
	else
		 TextStatus = L"Disabled"
		 TextToggle = L"Enable"
	end

	if not Ges.AbilityExists then
		TextAbility = L"Note: No Guard ability has currently been located"
	end

	local msg = L"\nDestination chat channel is: "..TextChannel
	msg = msg..L"\nYour current custom message is: "..towstring(Ges.Settings.Message)
	if SystemData.ActiveWindow.name == Ges.Button.Btn then
		msg = msg..L"\n\nLeft Click to "..TextToggle..L" Ges"
		msg = msg..L"\nRight Click to view the Options Window\n"
	end
	-- Set the tooltip message
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil)
	Tooltips.SetTooltipText (1, 1, L"Ges is currently "..TextStatus)
	if Ges.Enabled then
		Tooltips.SetTooltipColor(1, 1, 0, 255, 0)
	else
		Tooltips.SetTooltipColor(1, 1, 255, 0, 0)
	end
	Tooltips.SetTooltipText (2, 1, TextAbility)
	Tooltips.SetTooltipText (3, 1, towstring(msg))
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOM)
	Tooltips.Finalize()
	Ges.DebugMsg(L"Leaving ToggleButtonMouseOver",3)
end

function Ges.ExtrasSetAnchor(childWindow, parentWindow, positionSelected, offsetX, offsetY)
	Ges.DebugMsg(L"Entering ExtrasSetAnchor",3)
	WindowClearAnchors(childWindow)
	WindowAddAnchor( childWindow
	               , GesConstants.Anchors[positionSelected].name
	               , parentWindow
	               , GesConstants.Anchors[positionSelected].opposite
	               , tonumber(GesConstants.Anchors[positionSelected].offsetX + offsetX)
	               , tonumber(GesConstants.Anchors[positionSelected].offsetY + offsetY)
	               )
	Ges.DebugMsg(L"Leaving ExtrasSetAnchor",3)
end

-------------------------------
--- Monitor Icon Functions  ---
-------------------------------
function Ges.MonitorIconInit()
	Ges.DebugMsg(L"Entering MonitorIconInit",3)
	-- Create and register the toggle button window and then show/hide it based on the settings.
	CreateWindow(Ges.Monitor.Window, true)
	WindowSetShowing(Ges.Monitor.Window,false)
	WindowSetMovable(Ges.Monitor.Window, Ges.Settings.Monitor.Movable)
	-- Register window with the layout editor so that it can be moved and resized there.
	LayoutEditor.RegisterWindow ( Ges.Monitor.Window      -- windowName
                                , L"Ges Monitor"          -- windowDisplayName
                                , L"The Ges Monitor icon" -- windowDesc
                                , false                      -- allowSizeWidth
                                , false                      -- allowSizeHeight
                                , true                       -- allowHiding
                                , nil                        -- allowableAnchorList
                                )
	-- Cycle through the optional extras and create a working version for the GUI options to work with
	for i,v in ipairs(Ges.Monitor.Extras) do
		v.enabled = Ges.Settings.Monitor.Extras[i].enabled
		v.anchor  = Ges.Settings.Monitor.Extras[i].anchor
		v.offsetX = Ges.Settings.Monitor.Extras[i].offsetX
		v.offsetY = Ges.Settings.Monitor.Extras[i].offsetY
		Ges.ExtrasSetAnchor(v.name, Ges.Monitor.Window, v.anchor, v.offsetX, v.offsetY)
	end
	Ges.DebugMsg(L"Leaving MonitorIconInit",3)
end

function Ges.MonitorIconSetTextures()
	Ges.DebugMsg(L"Entering MonitorIconSetTextures",3)
	if not Ges.Settings.Monitor.Enabled or not Ges.Target.isActive then return end
	-- Set the background texture and slice for the Monitor Icon
	DynamicImageSetTexture( Ges.Monitor.Image.Name
	                      , GesConstants.IconsBackgrounds[Ges.Monitor.Image.Current].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( Ges.Monitor.Image.Name
	                           , GesConstants.IconsBackgrounds[Ges.Monitor.Image.Current].slice
	                           )
	-- Set the foreground texture and slice for the Monitor Icon
	DynamicImageSetTexture( Ges.Monitor.Hilight.Name
	                      , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_HILIGHT].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( Ges.Monitor.Hilight.Name
	                           , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_HILIGHT].slice
	                           )
	-- Set the status texture and slice for the Monitor Icon
	DynamicImageSetTexture( Ges.Monitor.Status.Name
	                      , GesConstants.IconsForegrounds[Ges.Monitor.Status.Current].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( Ges.Monitor.Status.Name
	                           , GesConstants.IconsForegrounds[Ges.Monitor.Status.Current].slice
										)
	if Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_CAREER].enabled then
		Ges.DebugMsg(L"MIST: Career Icon Enabled",2)
		DynamicImageSetTexture( Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_CAREER].name
		                      , Ges.Target.CareerTexture
		                      , Ges.Target.CareerOffsetX
		                      , Ges.Target.CareerOffsetY
		                      )
	else
		DynamicImageSetTexture(Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_CAREER].name,"",0,0)
	end
	if Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_HEALTH].enabled then
		Ges.DebugMsg(L"MIST: Health Text Enabled",2)
		LabelSetText(Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_HEALTH].name, towstring(Ges.Target.HealthPerc))
	else
		LabelSetText(Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_HEALTH].name, L"")
	end
	if Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_NAME].enabled then
		Ges.DebugMsg(L"MIST: Target Name Enabled",2)
		LabelSetText(Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_NAME].name, towstring(Ges.Target.Name))
	else
		LabelSetText(Ges.Monitor.Extras[GesConstants.EXTRAS_TARGET_NAME].name, L"")
	end
	Ges.DebugMsg(L"Leaving MonitorIconSetTextures",3)
end

function Ges.MonitorIconActivate()
	Ges.DebugMsg(L"Entering MonitorIconActivate",3)
	-- Check if Monitor Icon is enabled, if not return
	if not Ges.Settings.Monitor.Enabled then return end
	-- Show the monitor window
	WindowSetShowing(Ges.Monitor.Window,true)
	Ges.DebugMsg(L"Leaving MonitorIconActivate",3)
end

function Ges.MonitorIconDeactivate()
	Ges.DebugMsg(L"Entering MonitorIconDeactivate",3)
	-- Hide the monitor window
	WindowSetShowing(Ges.Monitor.Window,false)
	Ges.DebugMsg(L"Leaving MonitorIconDeactivate",3)
end

function Ges.MonitorIconUpdate()
	Ges.DebugMsg(L"Entering MonitorIconUpdate",3)
	if not Ges.Settings.Monitor.Enabled or not Ges.Target.isActive then return end

	-- Update Monitor Icon value based on targets status
	Ges.Monitor.Status.Current = GesConstants.FOREGROUND_BLANK
	if Ges.Target.isDead then
		Ges.Monitor.Image.Current = Ges.Settings.Monitor.DeadIcon
		Ges.Monitor.Status.Current = GesConstants.FOREGROUND_SKULL
	elseif not Ges.Target.isOnline then
		Ges.Monitor.Image.Current = Ges.Settings.Monitor.isOfflineIcon
		Ges.Monitor.Status.Current = GesConstants.FOREGROUND_TEXTDC
	elseif Ges.Settings.Monitor.Health.Enabled then
		local targetHealth  = tonumber(tostring(Ges.Target.HealthPerc))
		local thresholdHigh = tonumber(tostring(Ges.Settings.Monitor.Health.HighPerc))
		local thresholdMed  = tonumber(tostring(Ges.Settings.Monitor.Health.MedPerc))
		local thresholdLow  = tonumber(tostring(Ges.Settings.Monitor.Health.MinPerc))
		if targetHealth > thresholdHigh then
			Ges.Monitor.Image.Current = Ges.Settings.Monitor.Health.FullIcon
		elseif targetHealth > thresholdMed then
			Ges.Monitor.Image.Current = Ges.Settings.Monitor.Health.HighIcon
		elseif targetHealth > thresholdLow then
			Ges.Monitor.Image.Current = Ges.Settings.Monitor.Health.MedIcon
		else
			Ges.Monitor.Image.Current = Ges.Settings.Monitor.Health.MinIcon
		end
	else
		Ges.Monitor.Image.Current = Ges.Settings.Monitor.DefaultIcon
	end
	Ges.MonitorIconSetTextures()
	Ges.DebugMsg(L"Leaving MonitorIconUpdate",3)
end

function Ges.MonitorIconToggle(isEnabled)
	Ges.DebugMsg(L"Entering MonitorIconToggle",3)
	Ges.Settings.Monitor.Enabled = isEnabled
	if Ges.Buff.Active then
		if Ges.Settings.Monitor.Enabled then
			Ges.MonitorIconActivate()
		else
			Ges.MonitorIconDeactivate()
		end
	end
	Ges.DebugMsg(L"Leaving MonitorIconToggle",3)
end
------------------------------
--- Tracker Icon Functions ---
------------------------------
function Ges.TrackerIconInit()
	Ges.DebugMsg(L"Entering TrackerIconInit",3)
	-- Create and hide the tracker anchor window
	CreateWindow(Ges.Tracker.Anchor, true)
	WindowSetShowing(Ges.Tracker.Anchor,true)
	-- Create and hide the tracker anchor window
	CreateWindow(Ges.Tracker.Window, true)
	WindowSetShowing(Ges.Tracker.Window,false)
	WindowSetParent (Ges.Tracker.Window, Ges.Tracker.Anchor )
	-- Cycle through the optional extras and create a working version for the GUI options to work with
	for i,v in ipairs(Ges.Tracker.Extras) do
		v.enabled = Ges.Settings.Tracker.Extras[i].enabled
		v.anchor  = Ges.Settings.Tracker.Extras[i].anchor
		v.offsetX = Ges.Settings.Tracker.Extras[i].offsetX
		v.offsetY = Ges.Settings.Tracker.Extras[i].offsetY
		Ges.ExtrasSetAnchor(v.name, Ges.Tracker.Window, v.anchor, v.offsetX, v.offsetY)
	end
	-- Set the Tracker Icon anchor to the stored version.
	Ges.TrackerIconSetAnchor(Ges.Settings.Tracker.OffsetX, Ges.Settings.Tracker.OffsetY)
	Ges.TrackerIconLayoutInit()
	Ges.DebugMsg(L"Leaving TrackerIconInit",3)
end

function Ges.TrackerIconLayoutInit()
	Ges.DebugMsg(L"Entering TrackerIconLayoutInit",3)
	-- Create a layout version of the tracker icon so the Layout editor doesn't interfere with the AttachWindowToObject function
	CreateWindow(Ges.Tracker.Layout, true)
	WindowSetShowing(Ges.Tracker.Layout,false)
	-- Set the size of the layout version to be the same as the actual Tracker Icon
	WindowSetDimensions(Ges.Tracker.Layout, WindowGetDimensions(Ges.Tracker.Window))
	WindowSetScale     (Ges.Tracker.Layout, WindowGetScale     (Ges.Tracker.Window))
	WindowSetAlpha     (Ges.Tracker.Layout, WindowGetAlpha     (Ges.Tracker.Window))
	-- Anchor the layout version to the actual icon so that it appear above it in the layout editor
	WindowClearAnchors (Ges.Tracker.Layout)
	WindowAddAnchor(Ges.Tracker.Layout, "topleft", Ges.Tracker.Window, "topleft", 0, 0)
	-- Register window with the layout editor so that it can be moved and resized there.
	LayoutEditor.RegisterWindow ( Ges.Tracker.Layout     -- windowName
                               , L"Ges Tracker"          -- windowDisplayName
                               , L"The Ges Tracker icon" -- windowDesc
                               , false                   -- allowSizeWidth
                               , false                   -- allowSizeHeight
                               , true                    -- allowHiding
                               , nil                     -- allowableAnchorList
                               )
	Ges.DebugMsg(L"Leaving TrackerIconLayoutInit",3)
end

function Ges.TrackerIconLayoutUpdate(status)
	Ges.DebugMsg(L"Entering TrackerIconLayoutUpdate",3)
	if status == LayoutEditor.EDITING_END then
		local isShowing = WindowGetShowing(Ges.Tracker.Window)
		-- Resize the actual Tracker Icon to be the same as the Layout version.
		WindowSetShowing   (Ges.Tracker.Window, false)
		WindowSetDimensions(Ges.Tracker.Window, WindowGetDimensions(Ges.Tracker.Layout))
		WindowSetScale     (Ges.Tracker.Window, WindowGetScale     (Ges.Tracker.Layout))
		WindowSetAlpha     (Ges.Tracker.Window, WindowGetAlpha     (Ges.Tracker.Layout))
		WindowSetShowing   (Ges.Tracker.Window, isShowing)
		-- Re-anchor the Layout version to the actual icon
		WindowClearAnchors(Ges.Tracker.Layout)
		WindowAddAnchor   (Ges.Tracker.Layout, "topleft", Ges.Tracker.Window, "topleft", 0, 0)
	end
	Ges.DebugMsg(L"Leaving TrackerIconLayoutUpdate",3)
end

function Ges.TrackerIconSetAnchor(OffsetX, OffsetY)
	Ges.DebugMsg(L"Entering TrackerIconSetAnchor",3)
	WindowClearAnchors(Ges.Tracker.Window)
	WindowAddAnchor( Ges.Tracker.Window, "center"
	               , Ges.Tracker.Anchor, "center"
	               , tonumber(OffsetX)
	               , tonumber(OffsetY)
	               )
	Ges.DebugMsg(L"Leaving TrackerIconSetAnchor",3)
end


function Ges.TrackerIconSetTextures()
	Ges.DebugMsg(L"Entering TrackerIconSetTextures",3)
	if not Ges.Settings.Tracker.Enabled or not Ges.Target.isActive then return end
	-- Set the background texture and slice for the Tracker Icon
	DynamicImageSetTexture( Ges.Tracker.Image.Name
	                      , GesConstants.IconsBackgrounds[Ges.Tracker.Image.Current].texture
								 , 0, 0
								 )
	DynamicImageSetTextureSlice( Ges.Tracker.Image.Name
	                           , GesConstants.IconsBackgrounds[Ges.Tracker.Image.Current].slice
										)
	-- Set the foreground texture and slice for the Tracker Icon
	DynamicImageSetTexture( Ges.Tracker.Hilight.Name
	                      , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_HILIGHT].texture
								 , 0, 0
								 )
	DynamicImageSetTextureSlice( Ges.Tracker.Hilight.Name
	                           , GesConstants.IconsForegrounds[GesConstants.FOREGROUND_HILIGHT].slice
										)
	-- Set the status texture and slice for the Tracker Icon
	DynamicImageSetTexture( Ges.Tracker.Status.Name
	                      , GesConstants.IconsForegrounds[Ges.Tracker.Status.Current].texture
								 , 0, 0
								 )
	DynamicImageSetTextureSlice( Ges.Tracker.Status.Name
	                           , GesConstants.IconsForegrounds[Ges.Tracker.Status.Current].slice
										)
	-- Show the Career Icon if it is enabled
	if Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_CAREER].enabled then
		Ges.DebugMsg(L"TIST: Career Icon Enabled",2)
		DynamicImageSetTexture( Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_CAREER].name
		                      , Ges.Target.CareerTexture
		                      , Ges.Target.CareerOffsetX
		                      , Ges.Target.CareerOffsetY
		                      )
	else
		DynamicImageSetTexture(Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_CAREER].name,"",0,0)
	end
	-- Show the Health Text if it is enabled
	if Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_HEALTH].enabled then
		Ges.DebugMsg(L"TIST: Health Text Enabled",2)
		LabelSetText(Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_HEALTH].name, towstring(Ges.Target.HealthPerc))
	else
		LabelSetText(Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_HEALTH].name, L"")
	end
	-- Show the Target Name if it is enabled
	if Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_NAME].enabled then
		Ges.DebugMsg(L"TIST: Target Name Enabled",2)
		LabelSetText(Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_NAME].name, towstring(Ges.Target.Name))
	else
		LabelSetText(Ges.Tracker.Extras[GesConstants.EXTRAS_TARGET_NAME].name, L"")
	end
	Ges.DebugMsg(L"Leaving TrackerIconSetTextures",3)
end

function Ges.TrackerIconActivate(newEntityId)
	Ges.DebugMsg(L"Entering TrackerIconActivate",3)
	-- Check if Tracker Icon is enabled, if not return
	if not Ges.Settings.Tracker.Enabled then return end
	-- Show the Tracker window and attach the Tracker Anchor to the target
	WindowSetShowing(Ges.Tracker.Window,true)
	Ges.DebugMsg("TrackerIconActivate: Attaching to World Object ["..newEntityId.."]",3)
	AttachWindowToWorldObject(Ges.Tracker.Anchor, newEntityId)
	Ges.Tracker.EntityId = newEntityId
	Ges.Tracker.Active = true
	Ges.DebugMsg(L"Leaving TrackerIconActivate",3)
end

function Ges.TrackerIconDeactivate()
	Ges.DebugMsg(L"Entering TrackerIconDeactivate",3)
	-- Hide the Tracker window and detach the Tracker Anchor from the target
	if Ges.Tracker.Active then
		WindowSetShowing(Ges.Tracker.Window,false)
		DetachWindowFromWorldObject(Ges.Tracker.Anchor, Ges.Tracker.EntityId)
		Ges.Tracker.Active = false
		Ges.Tracker.EntityId = 0
	end
	Ges.DebugMsg(L"Leaving TrackerIconDeactivate",3)
end

function Ges.TrackerIconUpdate()
	Ges.DebugMsg(L"Entering TrackerIconUpdate",3)
	if not Ges.Settings.Tracker.Enabled or not Ges.Target.isActive then return end
	-- This code caters for the situations where the Party/Warband data for out target's entity id is set to 0 when
	-- it is no longer drawable but sometimes doesn't reset back to the actual entity id when it becomes drawable.
	if Ges.Target.EntityIdOld ~= Ges.Target.EntityId then
		Ges.TrackerIconDeactivate()
		if Ges.Target.EntityId ~= 0 then
			Ges.TrackerIconActivate(Ges.Target.EntityId)
		elseif not Ges.Target.isDistant then
			Ges.TrackerIconActivate(Ges.Target.EntityIdOld)
		end
	end
	-- Update Tracker Icon value based on targets status
	Ges.Tracker.Status.Current = GesConstants.FOREGROUND_BLANK
	if Ges.Target.isDead then
		Ges.Tracker.Image.Current = Ges.Settings.Tracker.DeadIcon
		Ges.Tracker.Status.Current = GesConstants.FOREGROUND_SKULL
	elseif not Ges.Target.isOnline then
		Ges.Tracker.Image.Current  = Ges.Settings.Tracker.isOfflineIcon
		Ges.Tracker.Status.Current = GesConstants.FOREGROUND_TEXTDC
	elseif Ges.Settings.Tracker.Health.Enabled then
		Ges.DebugMsg(L"TIU: Health Icon Enabled [true]",2)
		local targetHealth  = tonumber(tostring(Ges.Target.HealthPerc))
		local thresholdHigh = tonumber(tostring(Ges.Settings.Tracker.Health.HighPerc))
		local thresholdMed  = tonumber(tostring(Ges.Settings.Tracker.Health.MedPerc))
		local thresholdLow  = tonumber(tostring(Ges.Settings.Tracker.Health.MinPerc))
		if targetHealth > thresholdHigh then
			Ges.Tracker.Image.Current = Ges.Settings.Tracker.Health.FullIcon
		elseif targetHealth > thresholdMed then
			Ges.Tracker.Image.Current = Ges.Settings.Tracker.Health.HighIcon
		elseif targetHealth > thresholdLow then
			Ges.Tracker.Image.Current = Ges.Settings.Tracker.Health.MedIcon
		else
			Ges.Tracker.Image.Current = Ges.Settings.Tracker.Health.MinIcon
		end
	else
		Ges.Tracker.Image.Current = Ges.Settings.Tracker.DefaultIcon
	end
	Ges.TrackerIconSetTextures()
	Ges.DebugMsg(L"Leaving TrackerIconUpdate",3)
end

function Ges.TrackerIconToggle(isEnabled)
	Ges.DebugMsg(L"Entering TrackerIconToggle",3)
	Ges.Settings.Tracker.Enabled = isEnabled
	if Ges.Buff.Active then
		if Ges.Settings.Tracker.Enabled then
			Ges.TrackerIconUpdate()
		else
			Ges.TrackerIconDeactivate()
		end
	end
	Ges.DebugMsg(L"Leaving TrackerIconToggle",3)
end

-----------------------
--- Sound Functions ---
-----------------------
function Ges.SoundsInit()
	Ges.DebugMsg(L"Entering SoundsInit",3)
	for i,v in ipairs (Ges.Settings.Sounds.Preferences) do
		Ges.Sounds.Preferences[i].id = v.id
		Ges.Sounds.Preferences[i].threshold = v.threshold
		Ges.Sounds.Preferences[i].played = false
	end
	Ges.Sounds.AppliedId = GesConstants.SoundTypes[Ges.Settings.Sounds.Applied].id
	Ges.Sounds.RemovedId = GesConstants.SoundTypes[Ges.Settings.Sounds.Removed].id
	Ges.DebugMsg(L"Leaving SoundsInit",3)
end

function Ges.SoundsApplied()
	Ges.DebugMsg(L"Entering SoundsApplied",3)
	if Ges.Sounds.AppliedId ~= nil and Ges.Settings.Sounds.Enabled then
		Ges.SoundsPlay(Ges.Sounds.AppliedId)
	end
	Ges.DebugMsg(L"Leaving SoundsApplied",3)
end

function Ges.SoundsRemoved()
	Ges.DebugMsg(L"Entering SoundsRemoved",3)
	if Ges.Sounds.RemovedId ~= nil and Ges.Settings.Sounds.Enabled then
		Ges.SoundsPlay(Ges.Sounds.RemovedId)
	end
	Ges.DebugMsg(L"Leaving SoundsRemoved",3)
end

function Ges.SoundsUpdate()
	Ges.DebugMsg(L"Entering SoundsUpdate",3)
	if not Ges.Settings.Sounds.Enabled then Ges.DebugMsg(L"Sounds Disabled",3) return end
	Ges.DebugMsg("SU - healthDecrease ["..boolToString(Ges.Target.healthDecrease).."]",2)
	for i,v in ipairs (Ges.Sounds.Preferences) do
		if v.id ~= nil then
			Ges.DebugMsg("SU("..i..") - played ["..boolToString(v.played).."]",2)
			Ges.DebugMsg("SU("..i..") - threshold ["..v.threshold.."]",2)
			if Ges.Target.healthDecrease and not v.played and v.threshold >= Ges.Target.HealthPerc then
				v.played = true
				Ges.SoundsPlay(v.id)
			elseif not Ges.Target.healthDecrease and v.played and v.threshold <= Ges.Target.HealthPerc then
				v.played = false
			end
		end
	end
	Ges.DebugMsg(L"Leaving SoundsUpdate",3)
end

function Ges.SoundsPlay(soundId)
	Ges.DebugMsg(L"Entering SoundsPlay",3)
	Sound.Play(soundId)
	Ges.DebugMsg(L"Leaving SoundsPlay",3)
end

function Ges.SoundsReset()
	Ges.DebugMsg(L"Entering SoundsReset",3)
	for i,v in ipairs (Ges.Sounds.Preferences) do
		if v.id ~= nil then
			v.played = false
		end
	end
	Ges.DebugMsg(L"Leaving SoundsReset",3)
end
